package com.update.push.tool.core;

import java.util.Calendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class AlarmHelper 
{
	
	public static final int ALARM_TIMEOUT = 24*60*60*1000;
	public static final int ALARM_CHECK_TIMEOUT = 5;
	public static final int ALARM_TYPE_REGULAR = 659;
	public static final int ALARM_TYPE_CHECK_PENDING = 670;
	
	public static final String DefaultResquestTime = "21:30:0"; 
	public static int REQUEST_DAY_OF_YEAR = 1;
	public static int REQUEST_HOUR_OF_DAY = 21;
	public static int REQUEST_MINUTE = 30;
	public static int REQUEST_SECOND = 0;
	public static int REQUEST_MILLISECOND = 0;
	
	public static void setNextRegularTask(Context context,String requestTime)
	{
		Log.d("AlarmHelper","---setNextRegularTask");
		if(requestTime != null && !("".equals(requestTime)))
		{
			try 
			{
				String[] arrTime =  requestTime.split(":");
				REQUEST_DAY_OF_YEAR = Integer.parseInt(arrTime[0]);
				REQUEST_MINUTE = Integer.parseInt(arrTime[1]);
				REQUEST_SECOND = Integer.parseInt(arrTime[2]);
				Log.d("AlarmHelper", "---setNextRegularTask  h="+REQUEST_DAY_OF_YEAR+"   m="+REQUEST_MINUTE+"  sec="+REQUEST_SECOND);
			} catch (Exception e) 
			{
				REQUEST_HOUR_OF_DAY = 21;
				REQUEST_MINUTE = 30;
				REQUEST_SECOND = 0;
				e.printStackTrace();
			}
		}
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(System.currentTimeMillis());
		cal.add(Calendar.DAY_OF_YEAR, REQUEST_DAY_OF_YEAR);
	    cal.set(Calendar.HOUR_OF_DAY, REQUEST_HOUR_OF_DAY);
	    cal.set(Calendar.MINUTE, REQUEST_MINUTE);
	    cal.set(Calendar.SECOND, REQUEST_SECOND);
	    cal.set(Calendar.MILLISECOND, REQUEST_MILLISECOND);
		AlarmManager mgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(UpdaterReceiver.UPDATER_ALARM_RECEIVED);
		intent.putExtra("alarm_type", ALARM_TYPE_REGULAR);
		intent.setClass(context, UpdaterReceiver.class);
		PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
		mgr.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), ALARM_TIMEOUT, pi);
	}
	
	public static void setNextCheckPending(Context context, int minutesToDelay)
	{
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(System.currentTimeMillis());
		cal.add(Calendar.MINUTE, minutesToDelay);
		AlarmManager mgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(UpdaterReceiver.UPDATER_ALARM_RECEIVED);
		intent.putExtra("alarm_type", ALARM_TYPE_CHECK_PENDING);
		intent.setClass(context, UpdaterReceiver.class);
		PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
		mgr.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pi);
	}
	
	public static void setNextCheckPending(Context context)
	{
		Log.e("AlarmHelper", "---------AlarmHelper setNextCheckPending");
		setNextCheckPending(context, ALARM_CHECK_TIMEOUT);
	}
}
