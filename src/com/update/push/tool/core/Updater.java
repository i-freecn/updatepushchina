package com.update.push.tool.core;

import java.util.UUID;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;

import com.update.push.tool.core.UpdateDialog.Style;
import com.update.push.tool.task.BasicTask;
import com.update.push.tool.task.CheckKilledDownloads;
import com.update.push.tool.task.DownloadFileTask;
import com.update.push.tool.task.DownloadParamsTask;
import com.update.push.tool.task.FromNotifTask;
import com.update.push.tool.task.NetworkRequest;
import com.update.push.tool.task.ParseServerDataTask;
import com.update.push.tool.task.RequestServerTask;
import com.update.push.tool.utils.DataSaver;
import com.update.push.tool.utils.FileUtils;
import com.update.push.tool.utils.NetworkUtils;
import com.update.push.tool.utils.R_util;

public class Updater
{
	public static final String UpdateVersion = "4.0";
	public static final String CMD_0 = "0"; //General request
	public static final String CMD_1 = "1"; //push ???
	public static final String CMD_2 = "2"; //dialog btn no
	public static final String CMD_3 = "3"; //dialog
	public static final String CMD_4 = "4"; //download complete
	
	public static final int PUSH_TYPE_ERROR = -1;
	public static final int PUSH_TYPE_APK = 0;
	public static final int PUSH_TYPE_TEXT = 1;
	
//	public static String DataAddress= "http://ifcloud.cn:8081/UserPool/userPool/push/data";
	public static String DataAddress= "http://ifcloud.cn:8081/UserPool/userPool/push/newdata";
	public static String StatisticsAddress= "http://ifcloud.cn:8081/UserPool/userPool/push/count";
	
	private static Updater mUpdater;
	
	private Context mContext;
	private String mProductId;
	private String mChannelId;
	private String silentLoadUpdateApkGuid;
	private String silentLoadPushApkGuid;
	
	private String gameUserid = "";
	
	public static final int DETECTION_DIALOG_FLAG = 1;
	public static final int DETECTION_NOTIFICATION_FLAG = 0;
	
	public static boolean TEST = false;
	
	
	/**
	 * Initialize updater library
	 * @param context Activity context
	 * @param productid Product ID
	 * @param channelid Channel ID
	 * @param isChinaUnicom Is it ChinaUnicom?
	 */
	public static final void init(Context context,String productid,String channelid,String gameUserid)
	{
		get().mContext = context;
		get().mProductId = productid;
		get().mChannelId = channelid;
		get().gameUserid = gameUserid;
		DataSaver.saveProductId(context, productid);
		DataSaver.saveChannelId(context, channelid);
		DataSaver.saveGameUserid(context, gameUserid);
		String requesttime = DataSaver.getRequestNetTime(context, AlarmHelper.DefaultResquestTime);
		Log.d("Updater", "-------init request_time="+requesttime);
		AlarmHelper.setNextRegularTask(context,requesttime);
		DataSaver.saveDetectFlag(context, DETECTION_DIALOG_FLAG);
		get().sendRequest(DataAddress, CMD_0, gameUserid, UploadData.updateMessageType);
	}
	
	public static Updater get()
	{
		if(mUpdater == null)
		{
			mUpdater = new Updater();
		}
		return mUpdater;
	}
	
	public void changeContext(Context context)
	{
		if(mContext == null)
			mContext = context;
	}
	
	public Context getUpdaterContext()
	{
		return mContext;
	}
	
	/**
	 * Try to cancel task by it's guid
	 * @param guid task guid you want to cancel
	 */
	public void cancelTask(String guid)
	{
		Intent intent = new Intent(mContext, NetworkRequest.class);
		intent.putExtra("guid", guid);
		intent.setAction(BasicTask.CANCEL_TASK_ACTION);
		mContext.startService(intent);
	}
	
	/**
	 * Send request to server to get parameters
	 * @param address requested url
	 * @param command one of CMD_'x' variables in Updater class
	 * @param checkCode 
	 * @param msgType UploadData message type: update or push
	 */
	public String sendRequest(final String address, String command, String checkCode, String msgType)
	{
		final Intent intent = new Intent(mContext, NetworkRequest.class);
		String GUID = UUID.randomUUID().toString();
		intent.putExtra("guid", GUID);
		intent.putExtra("address", address);
		intent.putExtra("productId", DataSaver.getProductId(mContext, ""));
		intent.putExtra("channelId", DataSaver.getChannelId(mContext, ""));
		intent.putExtra("command", command);
		intent.putExtra("checkCode", checkCode);
		intent.putExtra("msgType", msgType);
		intent.setAction(RequestServerTask.TASK_ACTION);
		intent.putExtra(NetworkRequest.EXTRA_RESULT_RECEIVER, new ResultReceiver(new Handler()){
			@Override
			protected void onReceiveResult(int resultCode, Bundle resultData) 
			{
				if(resultCode == BasicTask.RESULT_OK)
				{
					String response = resultData.getString(BasicTask.RESULT_STRING);
					if(!address.equals(StatisticsAddress))
					{
						parseServerData(response);
					}
				}else if(resultCode == BasicTask.RESULT_FAIL)
				{
					addRequestToPending(RequestServerTask.TASK_ACTION, intent);
				}
			}
		});
		mContext.startService(intent);
		return GUID;
	}
	/**
	 * Send request to server to get parameters
	 * @param intent saved intent from RequestServerTask
	 */
	private String sendRequest(final Intent intent)
	{
		if(intent == null)return null;
		String address = intent.getStringExtra("address");
		String command = intent.getStringExtra("command");
		String checkCode = intent.getStringExtra("checkCode");
		String msgType = intent.getStringExtra("msgType");
		return sendRequest(address, command, checkCode, msgType);
	}
	
	/**
	 * Parse parameters from server
	 * @param data 'json-encoded to utf-8' string
	 */
	public String parseServerData(String data)
	{
		final Intent intent = new Intent(mContext, NetworkRequest.class);
		String GUID = UUID.randomUUID().toString();
		intent.putExtra("guid", GUID);
		intent.putExtra("data", data);
		intent.setAction(ParseServerDataTask.TASK_ACTION);
		intent.putExtra(NetworkRequest.EXTRA_RESULT_RECEIVER, new ResultReceiver(new Handler())
		{
			@Override
			protected void onReceiveResult(int resultCode, Bundle resultData)
			{
				if(resultCode == BasicTask.RESULT_OK)
				{
					downloadParams();
				}
				else if(resultCode == BasicTask.RESULT_FAIL)
				{
					addRequestToPending(ParseServerDataTask.TASK_ACTION, intent);
				}
			}
		});
		mContext.startService(intent);
		return GUID;
	}
	/**
	 * Parse parameters from server(pending version)
	 * @param intent saved intent from ParseServerDataTask
	 */
	private String parseServerData(final Intent intent){
		if(intent == null)return null;
		String data = intent.getStringExtra("data");
		return parseServerData(data);
	}
	
	/**
	 * Download parameters from server
	 * @param flag
	 */
	public String downloadParams()
	{
		final Intent intent = new Intent(mContext, NetworkRequest.class);
		String GUID = UUID.randomUUID().toString();
		intent.putExtra("guid", GUID);
		intent.setAction(DownloadParamsTask.TASK_ACTION);
		intent.putExtra(NetworkRequest.EXTRA_RESULT_RECEIVER, new ResultReceiver(new Handler())
		{
			@Override
			protected void onReceiveResult(int resultCode, Bundle resultData)
			{
				//pre-download only on wifi
				if(resultCode == BasicTask.RESULT_OK && NetworkUtils.isWiFi(mContext))
				{
					//download update apk
//					String apkUrl = DataSaver.getUpdateApkUrl(mContext, "");
//					if(!FileUtils.isApkInstalled(mContext, FileUtils.getNameFromUrl(apkUrl, ".apk"))){
//						String updateGuid = Updater.get().downloadFile(apkUrl, DataSaver.getVersionIcreased(mContext), UpdateNotification.UpdateFlag);
//					}
					
				}else if(resultCode == BasicTask.RESULT_UPDATE)
				{
					final String apkUrl = DataSaver.getUpdateApkUrl(mContext, "");
					if(FileUtils.isApkInstalled(mContext, FileUtils.getNameFromUrl(apkUrl, ".apk")))
					{
						//apk already installed
						return;
					}
					final int versionIncreased = DataSaver.getUpdateVersionIcreased(mContext);
					//notification params
					String title = DataSaver.getUpdateTitle(mContext, "");
					String message = DataSaver.getUpdateContent(mContext, "");
					Bitmap icon = (resultData.getString("bitmap") != null)?
							BitmapFactory.decodeFile(resultData.getString("bitmap")) :
							BitmapFactory.decodeResource(mContext.getResources(), R_util.instance(mContext).getDrawable("ic_launcher"));
					if(DataSaver.getDetectFlag(mContext, 0) == Updater.DETECTION_DIALOG_FLAG)
					{
						String styleString = DataSaver.getUpdateDialogStyle(mContext, "2");
						Style style = styleString.equals("1")?Style.ONE_BUTTON:Style.TWO_BUTTON;
						UpdateDialog dialog = Updater.UpdaterViewManager.getUpdateDialog(mContext, title, message, icon, style);
						dialog.show(new UpdateDialog.ButtonCallback() 
						{
							@Override
							public void onYesButton() 
							{
								if(versionIncreased >= 0)
								{
									DataSaver.saveUpdateLoadProductVersion(mContext, DataSaver.getUpdateCurrProductVersion(mContext, "0"));
								}
								Updater.get().uploadStats(Updater.CMD_1, DataSaver.getCheckCode(mContext, ""), UploadData.updateMessageType);
								Updater.get().downloadFile(apkUrl, versionIncreased, UpdateNotification.UpdateFlag);
							}
							
							@Override
							public void onNoButton() {
								Updater.get().uploadStats(Updater.CMD_2, DataSaver.getCheckCode(mContext, ""), UploadData.updateMessageType);
							}
						});
						Updater.get().uploadStats(Updater.CMD_3, DataSaver.getCheckCode(mContext, ""), UploadData.updateMessageType);
					}
				}
				else if(resultCode == BasicTask.RESULT_FAIL)
				{
					addRequestToPending(DownloadParamsTask.TASK_ACTION, intent);
				}
			}
		});
		mContext.startService(intent);
		return GUID;
	}
	/**
	 * Download parameters from server(pending version)
	 * @param intent saved intent from DownloadParamsTask
	 */
	private String downloadParams(final Intent intent){
		if(intent == null)return null;
		return downloadParams();
	}
	
	/**
	 * Download apk form apkUrl
	 * @param apkUrl url to download
	 * @param versionIncreased true, if version will be increased
	 * @param loadFlag UpdateNotification update/push/pushText/pushApk flag
	 */
	public String downloadFile(String apkUrl, int versionIncreased, int loadFlag)
	{
		final Intent intent = new Intent(mContext, NetworkRequest.class);
		String GUID = UUID.randomUUID().toString();
		intent.putExtra("guid", GUID);
		intent.putExtra("apkurl", apkUrl);
		intent.putExtra("version", versionIncreased);
		intent.putExtra("loadFlag", loadFlag);
		intent.setAction(DownloadFileTask.TASK_ACTION);
		intent.putExtra(NetworkRequest.EXTRA_RESULT_RECEIVER, new ResultReceiver(new Handler())
		{
			@Override
			protected void onReceiveResult(int resultCode, Bundle resultData) 
			{
				if(resultCode == BasicTask.RESULT_OK)
				{
					
				}
				else if(resultCode == BasicTask.RESULT_FAIL)
				{
					addRequestToPending(DownloadFileTask.TASK_ACTION, intent);
				}
			}
		});
		mContext.startService(intent);
		return GUID;
	}
	/**
	 * Download file(pending version)
	 * @param intent saved intent from DownloadFileTask
	 */
	private String downloadFile(final Intent intent){
		if(intent == null)return null;
		String apkUrl = intent.getStringExtra("apkurl");
		int versionIncreased = intent.getIntExtra("version", 0);
		int loadFlag = intent.getIntExtra("loadFlag", 0);
		return downloadFile(apkUrl, versionIncreased, loadFlag);
	}
	
	/**
	 * Upload statistics to server
	 * @param command 
	 * @param checkCode captcha check
	 * @param updateMessageType 
	 */
	public String uploadStats(String command, String checkCode, String updateMessageType){
		Log.e("Uploading stats", "command: " + command + ", updateType: " + updateMessageType + ", checkCode: " + checkCode);
		return get().sendRequest(StatisticsAddress, command, checkCode, updateMessageType);
	}
	
	/**
	 * Start file download from notification clicked
	 * @param notifFlag
	 * @param updateNotificationFlag
	 */
	public String downloadFromNotification(int notifFlag, int updateNotificationFlag){
		final Intent intent = new Intent(mContext, NetworkRequest.class);
		String GUID = UUID.randomUUID().toString();
		intent.putExtra("guid", GUID);
		intent.putExtra("notif_flag", notifFlag);
		intent.putExtra("update_flag", updateNotificationFlag);
		intent.setAction(FromNotifTask.TASK_ACTION);
		intent.putExtra(NetworkRequest.EXTRA_RESULT_RECEIVER, new ResultReceiver(new Handler()){
			@Override
			protected void onReceiveResult(int resultCode, Bundle resultData) {
				if(resultCode == BasicTask.RESULT_OK){
					
				}
				else if(resultCode == BasicTask.RESULT_FAIL){
					addRequestToPending(FromNotifTask.TASK_ACTION, intent);
				}
			}
		});
		mContext.startService(intent);
		return GUID;
	}
	/**
	 * Start file download from notification clicked(pending version)
	 * @param intent saved intent from FromNotifTask
	 */
	private String downloadFromNotification(final Intent intent){
		if(intent == null)return null;
		int notifFlag = intent.getIntExtra("notif_flag", 0);
		int updateNotificationFlag = intent.getIntExtra("update_flag", 0);
		return downloadFromNotification(notifFlag, updateNotificationFlag);
	}
	
	private String checkKilledDownloads(Context context){
		final Intent intent = new Intent(context, NetworkRequest.class);
		String GUID = UUID.randomUUID().toString();
		intent.putExtra("guid", GUID);
		intent.setAction(CheckKilledDownloads.TASK_ACTION);
		intent.putExtra(NetworkRequest.EXTRA_RESULT_RECEIVER, new ResultReceiver(new Handler()){
			@Override
			protected void onReceiveResult(int resultCode, Bundle resultData) {
				if(resultCode == BasicTask.RESULT_OK){
					
				}
				else if(resultCode == BasicTask.RESULT_FAIL){
					
				}
			}
		});
		mContext.startService(intent);
		return GUID;
	}
	
	//*******REQUESTS CONTROLS*****//
	/**
	 * Save intent for future. It will be used to retry task
	 * @param action associated action
	 * @param intent saved intent
	 */
	public void addRequestToPending(String action, Intent intent)
	{
		if(action.equals(RequestServerTask.TASK_ACTION))
		{
			new RequestServerTask().savePending(mContext, intent);
		}
		else if(action.equals(ParseServerDataTask.TASK_ACTION))
		{
			new ParseServerDataTask().savePending(mContext, intent);
		}
		else if(action.equals(DownloadParamsTask.TASK_ACTION))
		{
			new DownloadParamsTask().savePending(mContext, intent);
		}
		else if(action.equals(DownloadFileTask.TASK_ACTION))
		{
			new DownloadFileTask().savePending(mContext, intent);
		}
		else if(action.equals(FromNotifTask.TASK_ACTION))
		{
			new FromNotifTask().savePending(mContext, intent);
		}
		Log.e("Updater", "-----addRequestToPending action="+action);
		AlarmHelper.setNextCheckPending(mContext);
	}
	
	/**
	 * 
	 * Check if we have pending requests and schedule them
	 */
	public void queuePedingRequests()
	{
		Log.e("Updater", "------Pending requests Checking queuePedingRequests...");
		checkKilledDownloads(mContext);//yes
		sendRequest(new RequestServerTask().getPending(mContext));//yes
		parseServerData(new ParseServerDataTask().getPending(mContext));//yes
		downloadParams(new DownloadParamsTask().getPending(mContext));//no
		downloadFile(new DownloadFileTask().getPending(mContext));//yes
		downloadFromNotification(new FromNotifTask().getPending(mContext));//no
	}
	
	public static class UpdaterViewManager{
		private static UpdateDialog mUpdateDialog;
		private static UpdateNotification mUpdateNotification;
		private static UpdateNotification mPushNotification;
		private static UpdateNotification mPushTextNotification;
		
		public static final int UPDATE_NOTIFICATION_ID = 0;
		public static final int PUSH_NOTIFICATION_ID = 1;
		public static final int PUSHTEXT_NOTIFICATION_ID = 11;
		
		public static final UpdateDialog getUpdateDialog(Context context, String title, String message){
			
			mUpdateDialog = new UpdateDialog(context, title, message);
			
			return mUpdateDialog;
		}
		
		public static final UpdateDialog getUpdateDialog(Context context, String title, String message, Bitmap icon, UpdateDialog.Style style){
			
			mUpdateDialog = new UpdateDialog(context, title, message, icon, style);
			
			return mUpdateDialog;
		}
		
		public static final UpdateNotification getUpdateNotification(Context context, Bitmap icon, String title, String content, int notificationFlag){
			
			mUpdateNotification = new UpdateNotification(context, icon, UPDATE_NOTIFICATION_ID, title, content, notificationFlag);
			return mUpdateNotification;
		}
		
		public static final UpdateNotification getPushNotification(Context context, Bitmap icon, String title, String content, int notificationFlag){
			
			mPushNotification = new UpdateNotification(context, icon, PUSH_NOTIFICATION_ID, title, content, notificationFlag);
			
			return mPushNotification;
		}
		
		public static final UpdateNotification getPushTextNotification(Context context, Bitmap icon, String title, String content, int notificationFlag){
			
			mPushTextNotification = new UpdateNotification(context, icon, PUSHTEXT_NOTIFICATION_ID, title, content, notificationFlag);
			
			return mPushTextNotification;
		}
		
		public static final UpdateNotification getUpdateNotification()
		{
			Log.e("UpdaterViewManager", "-----------UpdaterViewManager mUpdateNotification="+mUpdateNotification);
			return mUpdateNotification;
		}
		
		public static final UpdateNotification getPushNotification()
		{
			Log.e("UpdaterViewManager", "-----------UpdaterViewManager mPushNotification="+mPushNotification);
			return mPushNotification;
		}
		
		public static final UpdateNotification getPushTextNotification(){
			return mPushTextNotification;
		}
	}
}
