package com.update.push.tool.core;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.util.Log;

import com.update.push.tool.utils.DataSaver;
import com.update.push.tool.utils.NetworkUtils;

public class UpdaterReceiver extends BroadcastReceiver
{
	
	public static final String UPDATER_ALARM_RECEIVED = "updater_alarm_receved_action";
	public static final String UPDATER_NOTIFICATION_UPDATE = "updater_notification_update_action";
	public static final String UPDATER_NOTIFICATION_PUSH = "updater_notification_push_action";

	@Override
	public void onReceive(Context context, Intent intent) 
	{
		String action = intent.getAction();
		Log.e("UpdaterReceiver", "---- Broadcast received action: " + action);
		Updater.get().changeContext(context);
		if(action.equals(UPDATER_ALARM_RECEIVED))//闹铃检测
		{
			Log.e("UpdaterReceiver", "---- Broadcast received action://闹铃检测" );
			//broadcast from alarm service, time to make server request
			int alarmType = intent.getIntExtra("alarm_type", 0);
			if(alarmType == AlarmHelper.ALARM_TYPE_REGULAR)//根据服务端的时间检测
			{
				Log.e("UpdaterReceiver", "---- Broadcast received action://根据服务端的时间检测" );
				DataSaver.saveDetectFlag(context, Updater.DETECTION_NOTIFICATION_FLAG);
				Updater.get().sendRequest(Updater.DataAddress, Updater.CMD_0,
						DataSaver.getCheckCode(context, ""), ServerData.updateMessageType);
			}
			else if(alarmType == AlarmHelper.ALARM_TYPE_CHECK_PENDING)//自动发起检测
			{
				Log.e("UpdaterReceiver", "---- Broadcast received action://自动发起检测" );
				if(NetworkUtils.isNetworkConnected(context)&&NetworkUtils.isWiFi(context))
				{
					Log.e("UpdaterReceiver", "---- Broadcast received action://自动发起检测 wifi" );
					//Updater.get().queuePedingRequests();
				}
			}
		}
		else if(action.equals(ConnectivityManager.CONNECTIVITY_ACTION))//网络状态发生变化检测
		{
			Log.e("UpdaterReceiver", "---- Broadcast received action: //网络状态发生变化检测" );
			//network change occured, time to check pending requests
			if(NetworkUtils.isNetworkConnected(context)&&NetworkUtils.isWiFi(context))
			{
				Log.e("UpdaterReceiver", "---- Broadcast received action: //网络状态发生变化检测 wifi" );
				DataSaver.saveDetectFlag(context, Updater.DETECTION_NOTIFICATION_FLAG);
				AlarmHelper.setNextCheckPending(context, 1);
			}
		}
		else if(action.equals(UPDATER_NOTIFICATION_UPDATE))//通知栏update点击操作
		{
			Log.e("UpdaterReceiver", "---- Broadcast received action: 通知栏update点击操作" );
			//user clicked notification with update flag
			Updater.get().downloadFromNotification(Updater.DETECTION_NOTIFICATION_FLAG, UpdateNotification.UpdateFlag);
		}
		else if(action.equals(UPDATER_NOTIFICATION_PUSH))//通知栏push点击操作
		{
			Log.e("UpdaterReceiver", "---- Broadcast received action: //通知栏push点击操作" );
			//user clicked notification with push flag
			Updater.get().downloadFromNotification(Updater.DETECTION_NOTIFICATION_FLAG, UpdateNotification.PushFlag);
		}
	}

}
