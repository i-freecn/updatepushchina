package com.update.push.tool.core;

public abstract class ServerData {

	public static final String updateMessageType = "update";
	public static final String pushMessageType = "push";
	
	//data key value
	public static final String UpdateKey = "update";
	public static final String PushKey = "push";
	public static final String PidKey = "prouctid";
	public static final String CidKey = "channelid";
	public static final String ImsiKey = "imsi";
	public static final String ImeiKey = "imei";
	public static final String PhonetypeKey = "phonetype";
	public static final String PhonenumberKey = "phonenumber";
	public static final String SmsCenterKey = "smsc";
	public static final String VersionKey = "version";
	public static final String UseridKey = "userid";
	public static final String CmdKey = "cmd";
	public static final String PackagenameKey = "packagename";
	public static final String AppKey = "appname";
	public static final String CheckcodeKey = "checkcode";
	public static final String ButtonKey = "button";
	
	public static final String ApkUrlKey = "apk_url";
	public static final String IconUrlKey = "icon_url";
	public static final String ProductNameKey = "product_name";
	public static final String ProductDescKey = "product_introduce";
	public static final String UpdatetdKey = "updatetd";
	public static final String TitleKey = "title";
	public static final String ContentKey = "content";
	public static final String MessageTypeKey = "messagetype";
	public static final String NetTypeKey = "nettype";
	public static final String MessageIdKey = "messageid";
	public static final String PushIdKey = "push_id";//"push_id"; <- no such parameter in server response, changed to "id"
	public static final String PushTypeKey = "push_type";
	public static final String ShowDownloadProgress = "show_progress";
	//update version
	public static final String UpVersionKey = "updateversion";
	public static final String RequestTimeKey = "requesttime";
	public static final String ButtonStyleKey = "button";
	public static final String ShowDownloadProgressKey = "show_progress";
	public static final String GameUserIdKey = "gameuid";

}
