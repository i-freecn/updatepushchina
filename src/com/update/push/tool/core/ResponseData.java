package com.update.push.tool.core;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.update.push.tool.utils.DataSaver;

public class ResponseData extends ServerData 
{
	private static final String DefaultPushGameName = "DefaultPushGameName";
	private static final String DefaultGameName = "DefaultGameName";
	
	private Context context;
	//update
	private String updateApkUrl;
	private String updateIconUrl;
	private String updateVersion;
	private String updateGameName;
	private String updateTitle;
	private String updateContent;
	private String updateId;
	private boolean updateShowProgress;
	private UpdateDialog.Style updateDialogStyle;
	
	//push
	private String pushApkUrl;
	private String pushIconUrl;
	private String pushVersion;
	private String pushGameName;
	private String pushTitle;
	private String pushContent;
	private int pushType;
	private String pushId;
	private boolean pushShowProgress = true;
	
	public ResponseData(Context context)
	{
		this.context = context;
	}
	
	public void saveUpdateTypeData(String updateData) throws JSONException{
		if(updateData == null || updateData.equals("") || updateData.equals("[]"))
		{
			updateApkUrl = "";//"http://mingxiangyun.dnion.com/JungleHeat_iFreeZY.apk";
			updateIconUrl = "";//"http://www.ifcloud.cn:8080/UserPool/static/uploads/2014/01/28/5370/100x100136.jpg";
			DataSaver.saveUpdateApkUrl(context, updateApkUrl);
			DataSaver.saveUpdateIconUrl(context, updateIconUrl);
//			updateVersion = "11";
//			updateTitle = "Title";
//			updateContent = "COntent";
//			updateId = "13";
//			updateShowProgress = true;
//			updateDialogStyle = UpdateDialog.Style.TWO_BUTTON;
//			DataSaver.saveUpdateCurrProductVersion(context, updateVersion);
//			DataSaver.saveUpdateTitle(context, updateTitle);
//			DataSaver.saveUpdateContent(context, updateContent);
//			DataSaver.saveUpdateShowProgress(context, true);
//			DataSaver.saveUpdateMessageID(context, updateId);
//			DataSaver.saveUpdateDialogStyle(context, "2");
		}else
		{
			JSONArray updateArray = new JSONArray(updateData);
			for(int i = 0; i < updateArray.length(); i++){
				JSONObject updateObject = updateArray.getJSONObject(i);
				updateApkUrl = updateObject.optString(ApkUrlKey, null);
				updateIconUrl = updateObject.optString(IconUrlKey, null);
				updateVersion = updateObject.optString(VersionKey, null);
				updateTitle = updateObject.optString(ProductNameKey, null);
				updateContent = updateObject.optString(ContentKey, null);
				updateId = updateObject.optString(UpdatetdKey, null);
				updateShowProgress = updateObject.optBoolean(ShowDownloadProgress, true); //<- no such parameter for now! must be implemented on server side
				String dialogStyle = updateObject.optString(ButtonKey, null);
				if(dialogStyle == null || dialogStyle.equals("")){
					updateDialogStyle = UpdateDialog.Style.TWO_BUTTON;
					DataSaver.saveUpdateDialogStyle(context, "2");
				}
				if(dialogStyle.equals("1")){
					updateDialogStyle = UpdateDialog.Style.ONE_BUTTON;
					DataSaver.saveUpdateDialogStyle(context, "1");
				}
				else{
					updateDialogStyle = UpdateDialog.Style.TWO_BUTTON;
					DataSaver.saveUpdateDialogStyle(context, "2");
				}
				
				DataSaver.saveUpdateApkUrl(context, updateApkUrl);
				DataSaver.saveUpdateIconUrl(context, updateIconUrl);
				DataSaver.saveUpdateCurrProductVersion(context, updateVersion);
				DataSaver.saveUpdateTitle(context, updateTitle);
				DataSaver.saveUpdateContent(context, updateContent);
				DataSaver.saveUpdateMessageID(context, updateId);
				DataSaver.saveUpdateShowProgress(context, updateShowProgress);
			}
		}
	}
	
	public void saveRequestNetTime(String requestTime)
	{
		DataSaver.saveRequestNetTime(context, requestTime);
	}
	
	public void saveUpdateUserId(String userId){
		DataSaver.saveUpdatePushUserId(context, userId);
	}
	
	public void savePushTypeData(String pushData) throws JSONException
	{
		if(pushData == null || pushData.equals("") || pushData.equals("[]"))
		{
			pushApkUrl = "";
			pushIconUrl = "";
			pushGameName = DefaultPushGameName;
			DataSaver.savePushCurrGameName(context, pushGameName);//保存当前游戏名
			DataSaver.savePushApkUrl(context, DefaultPushGameName,pushApkUrl);
			DataSaver.savePushIconUrl(context, DefaultPushGameName,pushIconUrl);
			DataSaver.savePushGameName(context, DefaultPushGameName,DefaultPushGameName);
		}
		else
		{
			JSONArray pushArray = new JSONArray(pushData);
			for(int i = 0; i < pushArray.length(); i++)
			{
				JSONObject pushObject = pushArray.getJSONObject(i);
				
				pushApkUrl = pushObject.optString(ApkUrlKey, null);
				pushIconUrl = pushObject.optString(IconUrlKey, null);
				pushVersion = pushObject.optString(VersionKey, null);
				pushGameName = pushObject.optString(ProductNameKey, null);
				pushTitle = pushObject.optString(TitleKey, null);
				pushContent = pushObject.optString(ContentKey, null);
				pushType = Integer.valueOf(pushObject.optString(PushTypeKey, Updater.PUSH_TYPE_ERROR+""));
				pushId = pushObject.optString(PushIdKey, null);
				pushShowProgress = pushObject.optBoolean(ShowDownloadProgress, true); //<- no such parameter for now! must be implemented on server side
				
				if("".equals(pushGameName) || pushGameName == null)
				{
					pushGameName = DefaultGameName;
				}
				
				if("".equals(pushVersion) || pushVersion == null)
				{
					pushVersion = "0";
				}
				
				DataSaver.savePushCurrGameName(context, pushGameName);//保存当前游戏名
				DataSaver.savePushApkUrl(context,pushGameName, pushApkUrl);
				DataSaver.savePushIconUrl(context,pushGameName, pushIconUrl);
				DataSaver.savePushCurrProductVersion(context,pushGameName, pushVersion);//push game version
				DataSaver.savePushGameName(context,pushGameName, pushGameName);//push game Name
				DataSaver.savePushTitle(context,pushGameName, pushTitle);
				DataSaver.savePushContent(context,pushGameName, pushContent);
				DataSaver.savePushID(context,pushGameName, pushId);
				DataSaver.savePushShowProgress(context,pushGameName, pushShowProgress);
				DataSaver.savePushType(context,pushGameName, pushType);
			}
		}
	}
	
	
}
