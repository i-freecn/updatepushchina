package com.update.push.tool.core;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.update.push.tool.utils.R_util;

public class UpdateDialog {
    //Dialog信息
    private String mTitle = "提示";
    private String mMessage = "更新";
    private String mConfirmBtn = "立即更新";
    private String mCancelBtn = "稍后提示";
    private Bitmap mIcon;
    private Context mContext;
    private Style mStyle;
    private Handler mHandler;
    
    private R_util r_util;

    public UpdateDialog(Context context, String title, String message, Bitmap icon, Style style) 
    {
    	r_util = R_util.instance(context);
        mContext = context;
        mIcon = icon;
        mTitle = title;
        mMessage = message;
        mStyle = style;
        mHandler = new Handler();
    }
    

    public UpdateDialog(Context context, String title, String message) {
        mContext = context;
        mTitle = title;
        mMessage = message;
        if (mTitle == null || "".equals(mTitle)) {
            mTitle = "提示";
        }

        if (mMessage == null || "".equals(mMessage)) {
            mMessage = "是否更新";
        }
    }

    private void showUpdateView(final ButtonCallback bck) {
        final Dialog dialog = new Dialog(mContext, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
//        final Dialog dialog = new Dialog(mContext);

        LayoutInflater inflater = LayoutInflater.from(mContext);
        View dialogView = inflater.inflate(r_util.getLayout("update_dialog"), null);
        ImageView icon = (ImageView) dialogView.findViewById(r_util.getID("update_icon"));
        TextView title = (TextView) dialogView.findViewById(r_util.getID("update_title"));
        TextView message = (TextView) dialogView.findViewById(r_util.getID("update_message"));
        Button left = (Button) dialogView.findViewById(r_util.getID("update_button_left"));
        Button right = (Button) dialogView.findViewById(r_util.getID("update_button_right"));
        
        title.setText(mTitle);
        message.setText(mMessage);
        left.setText(mConfirmBtn);
        right.setText(mCancelBtn);

        if (mIcon != null) {
            icon.setImageBitmap(mIcon);
        }

        if (mStyle == Style.ONE_BUTTON) {
            right.setVisibility(View.GONE);
        }

        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                bck.onYesButton();
                if (mIcon != null) 
                {
                	mIcon.recycle();
                }
            }
        });
        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                bck.onNoButton();
                if (mIcon != null) 
                {
                	mIcon.recycle();
                }
            }
        });
        
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.setCancelable(false);
        dialog.setContentView(dialogView);
        dialog.show();
    }

    public void show(final ButtonCallback bck) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                UpdateDialog.this.showUpdateView(bck);
            }
        });
    }

    public enum Style {
        ONE_BUTTON,
        TWO_BUTTON
    }

    public interface ButtonCallback {
        void onYesButton();
        void onNoButton();
    }
}
