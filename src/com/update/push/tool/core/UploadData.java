package com.update.push.tool.core;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONStringer;

import android.content.Context;
import android.util.Log;

import com.update.push.tool.utils.ActivityHelper;
import com.update.push.tool.utils.DataSaver;
import com.update.push.tool.utils.NetworkUtils;
import com.update.push.tool.utils.PhoneUtils;

public class UploadData extends ServerData
{
	private String productId;
	private String channelId;
	private String cmd;
	private String checkCode;
	private String messageType;
	
	private String imsi;
	private String imei;
	
	private String phoneType;
	private String appVersion;
	private String userId;
	private String messageId;
	
	private String netType;
	private ArrayList<String> packageName;

	private String phoneNumber;
	private String smsCenter;
	
	private String buttonStyle = "";
	private String showProgress = "";
	private String gameuid = "";
	
	public UploadData(Context context, String productId, String channelId, String command, String checkCode, String msgType){
		this.productId = productId;
		this.channelId = channelId;
		this.cmd = command;
		this.checkCode = checkCode;
		this.messageType = msgType;
		if(msgType.equals(updateMessageType))
		{
			messageId = DataSaver.getUpdateMessageID(context, "");
			showProgress = DataSaver.getUpdateShowProgress(context, true)+"";
		}else if(msgType.equals(pushMessageType))
		{
			String currentGN = DataSaver.getPushCurrGameName(context, "");
			Log.e("UploadData", "-----push currentGN="+currentGN);
			messageId = DataSaver.getPushID(context,currentGN, "");
			showProgress = DataSaver.getPushShowProgress(context, currentGN,true)+"";
		}
		
		imsi = PhoneUtils.getPhoneIMSI(context);
		imei = PhoneUtils.getPhoneIMEI(context);
		
		phoneType = PhoneUtils.getPhoneType();
		appVersion = ActivityHelper.getVersion(context);
		userId = PhoneUtils.getDeviceID(context);//DataSaver.getUpdateUserId(context, "") //change to unique user id
		netType = NetworkUtils.getNetworkTypeName(context);
		packageName = new ArrayList<String>();
		packageName = ActivityHelper.getPhoneInstalledAPK(context);
		phoneNumber = PhoneUtils.getPhoneNumber(context, imsi);
		buttonStyle = DataSaver.getUpdateDialogStyle(context, "-1");
		gameuid = DataSaver.getGameUserid(context, "");
		if(gameuid == null || gameuid.equals(""))
		{
			gameuid = checkCode;
		}
//		smsCenter = SmsCenter.getSmsInPhone(context);
		smsCenter = "";
	}
	
	public String createUploadData()
	{
		JSONStringer jsonTxt = new JSONStringer();
		try {
			jsonTxt.array();
			jsonTxt.object();
			//base values
			jsonTxt.key(PidKey);
			jsonTxt.value(productId);
			jsonTxt.key(CidKey);
			jsonTxt.value(channelId);
			jsonTxt.key(ImsiKey);
			jsonTxt.value(imsi);
			jsonTxt.key(ImeiKey);
			jsonTxt.value(imei);
			jsonTxt.key(PhonetypeKey);
			jsonTxt.value(phoneType);
			jsonTxt.key(PhonenumberKey);
			jsonTxt.value(phoneNumber);
			jsonTxt.key(SmsCenterKey);
			jsonTxt.value(smsCenter);
			jsonTxt.key(VersionKey);
			jsonTxt.value(appVersion);
			jsonTxt.key(UseridKey);
			jsonTxt.value(userId);
			jsonTxt.key(CmdKey);
			jsonTxt.value(cmd);
			jsonTxt.key(CheckcodeKey);
			jsonTxt.value(checkCode);
			jsonTxt.key(UpVersionKey);
			jsonTxt.value(Updater.UpdateVersion);
			jsonTxt.key(ButtonStyleKey);
			jsonTxt.value(buttonStyle);
			jsonTxt.key(ShowDownloadProgressKey);
			jsonTxt.value(showProgress);
			jsonTxt.key(GameUserIdKey);
			jsonTxt.value(gameuid);
			
			//message values
			jsonTxt.key(MessageIdKey);
			jsonTxt.value(messageId);
			jsonTxt.key(MessageTypeKey);
			jsonTxt.value(messageType);
			jsonTxt.key(NetTypeKey);
			jsonTxt.value(netType);
			
			//app values
			jsonTxt.key(PackagenameKey);
			jsonTxt.array();
			for(int x=0;x<packageName.size();x++)
			{
				jsonTxt.object();
				jsonTxt.key(AppKey);
				jsonTxt.value(packageName.get(x));
				jsonTxt.endObject();
			}
			jsonTxt.endArray();
			jsonTxt.endObject();
			jsonTxt.endArray();
		} catch (JSONException e) {
			e.printStackTrace();
		}
//		System.out.println("-----UploadData data="+jsonTxt.toString());
		return jsonTxt.toString();
	}
}
