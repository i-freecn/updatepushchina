package com.update.push.tool.core;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import com.update.push.tool.utils.R_util;

public class UpdateNotification 
{
	public static String Tag = "UpdateNotification";
	public static int UpdateFlag = 1;
	public static int PushFlag = 2;
	public static int PushTxtFlag = 3;
	public static int PushApkFlag = 4;
	private NotificationManager ntfManager;
	private NotificationCompat.Builder builder;
	private Notification ntf;
	private int nid;
	private PendingIntent pi;
	private PendingIntent confirmPI;
	
	private R_util r_util;
	
	public UpdateNotification(Context context,Bitmap notifyicon,int id,String title,String content,int ntfFlag)
	{
		//mContext = context;
		r_util = R_util.instance(context);
		nid = id;
		if(title == null || "".equals(title))
		{
			title = "提示更新";
		}
		if(notifyicon == null)
		{
			try 
			{
				notifyicon = BitmapFactory.decodeResource(context.getResources(), r_util.getDrawable("ic_launcher"));
				Log.e("UpdateNotification", "-----init icon11");
			} catch (Exception e) 
			{
				Log.e("UpdateNotification", "-----init icon Exception:"+e.getMessage());
			}
		}
		ntfManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		builder = new NotificationCompat.Builder(context);
		builder.setTicker(title);
		builder.setSmallIcon(r_util.getDrawable("ic_launcher"));
//		不同的手机会造成不适配
//		builder.setLargeIcon(notifyicon);
		builder.setWhen(System.currentTimeMillis());
//		builder.setSound(Notification.DEFAULT_SOUND);
//	         添加声音提示  
//		ntf.defaults=Notification.DEFAULT_SOUND;  
//		audioStreamType的值必须AudioManager中的值，代表着响铃的模式  
//		ntf.audioStreamType= android.media.AudioManager.ADJUST_LOWER; 
//		ntf.flags |= Notification.FLAG_AUTO_CANCEL;//点击通知后自动清除通知
		Intent in = new Intent();
		pi = PendingIntent.getBroadcast(context, 0, in, PendingIntent.FLAG_CANCEL_CURRENT);
//		TextView scroll
		RemoteViews rv = new RemoteViews(context.getPackageName(), r_util.getLayout("update_notificationview"));
		if(notifyicon != null)
		{
			Log.e("UpdateNotification", "-----init layout icon22");
			rv.setImageViewBitmap(r_util.getID("notifyIcon"), notifyicon);
		}
		Intent intent = new Intent();
		if(UpdateFlag == ntfFlag)
		{
			intent = new Intent(UpdaterReceiver.UPDATER_NOTIFICATION_UPDATE);
		}else if(PushFlag == ntfFlag)
		{
			intent = new Intent(UpdaterReceiver.UPDATER_NOTIFICATION_PUSH);
		}
		confirmPI = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		rv.setOnClickPendingIntent(r_util.getID("confirm"), confirmPI);
		if(content != null && !"".equals(content))
		{
			rv.setTextViewText(r_util.getID("progress"), content);
		}
		builder.setContent(rv);
		builder.setOngoing(false);
		builder.setAutoCancel(true);
		builder.setContentIntent(pi);
		ntf = builder.build();
		Log.e(Tag, "------UpdateNotification init over");
		
	}
	
	public void sendNotify()
	{
		ntfManager.notify(nid, ntf);
	}
	
	public void cancel()
	{
		ntfManager.cancel(nid);
	}
	
	public Notification getCurrNotification()
	{
		return ntf;
	}
	
	public void setContent(String content,int visibility)
	{
		if(ntf != null)
		{
			ntf = builder.setAutoCancel(true).setOngoing(false).build();
			ntf.contentView.setTextViewText(r_util.getID("progress"), content);
			ntf.contentView.setViewVisibility(r_util.getID("confirm"), visibility);
		}
	}
	
	public void setProgressVisible(boolean visible){
		if(ntf != null && visible){
			ntf = builder.setAutoCancel(false).setOngoing(true).build();
			ntf.contentView.setViewVisibility(r_util.getID("download"), View.VISIBLE);
			ntf.contentView.setViewVisibility(r_util.getID("text"), View.GONE);
		}
		else if(ntf != null && !visible){
			ntf = builder.setAutoCancel(true).setOngoing(false).build();
			ntf.contentView.setViewVisibility(r_util.getID("download"), View.GONE);
			ntf.contentView.setViewVisibility(r_util.getID("text"), View.VISIBLE);
			
		}
	}
	
	public void setProgress(long current, long total){
		if(ntf != null){
			int curr = (int)((float)current/total * 100);
			if(curr >= 100)
				curr = 100;
			ntf.contentView.setProgressBar(r_util.getID("progressBar"), 100, curr, false);
			sendNotify();
		}
	}
}
