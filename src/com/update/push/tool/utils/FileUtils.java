package com.update.push.tool.utils;

import java.io.File;
import java.io.FileOutputStream;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

public class FileUtils {

	public static String ROOT = Environment.getExternalStorageDirectory().getAbsolutePath()+"/";
	public static String UpdaterDirName = "UpdateDownloader";
	public static String UpdaterPushIconDirName = "UpdateIconDownloader";
	
	public static boolean isMediaAvailable()
	{  
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
			return true;
		}
		return false;
	}
	
	public static String saveBitmap(Context context, String name, Bitmap bitmap)
	{
		File tmpIconDir = new File(FileUtils.ROOT + FileUtils.UpdaterPushIconDirName);
		boolean bool = tmpIconDir.mkdirs();
		File file = new File(FileUtils.ROOT + FileUtils.UpdaterPushIconDirName, name);
		if(file.exists() && file.isDirectory())
		{
			file.delete();
		}
		//file.mkdirs();
		FileOutputStream os;
		try
		{
			os = new FileOutputStream(file);
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
			os.flush();
			os.close();
			return file.getAbsolutePath();
		}catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	//安装apk
	public static void installFile(Context context, String filePath)
	{
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setDataAndType(Uri.fromFile(new File(filePath)), "application/vnd.android.package-archive");
		context.startActivity(intent);
	}	
	// 打开APK程序
	public static void openFile(Context context, File file) 
	{
		Log.e("OpenFile", file.getName());
		Intent intent = new Intent();
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setAction(android.content.Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.fromFile(file),
				"application/vnd.android.package-archive");
		context.startActivity(intent);
	}
	
	/**
	 * Check if apk file has been installed
	 * @param context current context
	 * @param pathToApk absolute path to apk file on device
	 * @return true, if apk is installed, false otherwise
	 */
	public static boolean isApkInstalled(Context context, String pathToApk){
		PackageInfo info = context.getPackageManager().getPackageArchiveInfo(pathToApk, 0);
		if(info == null)
			return false;
		String packageName = info.packageName;

		PackageManager pm = context.getPackageManager();
		ApplicationInfo ai;
		try {
		     ai = pm.getApplicationInfo(packageName, 0);
		} catch (final NameNotFoundException e) {
		     ai = null;
		}
		
		return ai != null;
	}
	
	/**
	 * Get absolute path to file on device. This file might not actually exist.
	 * @param apkUrl File url
	 * @param suffix suffix of file(usually '.apk')
	 * @return absolute path to file on device
	 */
	public static String getNameFromUrl(String apkUrl, String suffix){
		if(apkUrl == null || apkUrl.equals(""))
			return null;
		String fileName = apkUrl.substring(apkUrl.lastIndexOf("/") + 1, apkUrl.lastIndexOf("."));
		File tmpDir = new File(FileUtils.ROOT + FileUtils.UpdaterDirName);
		tmpDir.mkdirs();
		File apkFile = new File(tmpDir+"/"+fileName+suffix);
		return apkFile.getAbsolutePath();
	}
}
