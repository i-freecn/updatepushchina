package com.update.push.tool.utils;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpParams;

import android.text.TextUtils;
/**
 * 用代理发请求获取手机号
 * 备注：必须用cmwap网络访问。
 * */
public class MyGetPhoneNumberUtil 
{//
	public static String sendRequest() 
	{
		// 10.0.0.172 (host) port(80) url
//		String GET_PHONENUM_URL = "http://wap.10086.cn/index.jsp";
		String GET_PHONENUM_URL = "http://wap.10086.cn/ywcx/yecx.jsp";
		String host = "10.0.0.172";
		int port = 80;
		String result = "";
		DefaultHttpClient httpclient = null;
		DataInputStream dis = null;
		try {
			httpclient = new DefaultHttpClient();
			if (!TextUtils.isEmpty(host) && port > 0) {
				HttpHost proxy = new HttpHost(host, port);
				HttpParams params = httpclient.getParams();
				params.setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
			}
			HttpGet httpGet = new HttpGet(GET_PHONENUM_URL);
			httpGet.setHeader("Accept",
					"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			httpGet.setHeader("Content-Type", "text/xml;charset=utf-8");
			httpGet.setHeader("Cache-Control", "no-cache");
			httpGet.setHeader("User-Agent",
					"NokiaN73-1/4.0850.43.1.1 Series60/3.0 Profile/MIDP-2.0 Configuration/CLDC-1.1");

			HttpResponse rsp = httpclient.execute(httpGet);

			CookieStore cookieStore = httpclient.getCookieStore();
			HttpEntity entity = rsp.getEntity();

			int status_code = rsp.getStatusLine().getStatusCode();
			if (status_code == 302 || status_code == 301) {
				Header header = rsp.getLastHeader("Location");
			} else if (rsp.getLastHeader("Content-Type") != null) {
				// 判断是否是移动资费页面，还是挺复杂
				Header contentType = rsp.getLastHeader("Content-Type");
				String type = contentType.getValue();
				if (type != null && type.contains("text/vnd.wap.wml")) {
					entity.consumeContent();
					if (httpclient != null) {
						httpclient.clearRequestInterceptors();
					}
				}
			}

			byte[] bytes = new byte[(int) entity.getContentLength()];
			dis = new DataInputStream(entity.getContent());
			dis.readFully(bytes);
			result = new String(bytes, "utf-8");
		} catch (IOException e) {
//			PatLogger.log_e(e);
		} finally {
			try {
				if (dis != null) {
					dis.close();
				}
			} catch (IOException e) {
//				PatLogger.log_e(e);
			}
			httpclient.getConnectionManager().shutdown();
		}
		return subStringPhoneNumber(result);
	}
	
	private static String subStringPhoneNumber(String xml){
//		//Log.d("xmg", "xml="+xml);//跨度，想下，能解决问题
		String result;
		try {
			String str1 = "<span class=\"bluebg\">";
			result = xml.substring(xml.indexOf(str1)+str1.length(), xml.indexOf("</span>"));
		} catch (Exception e) {
			return "";
		}
		return PhoneNunberParser(result);
	}
	//截取手机号码字符串	
	private static String PhoneNunberParser(String content){
		String phonenumber = "";
		if (content != null){
			String regex = "[^\\d]+(\\d{11})[^\\d]+";
			phonenumber = matchPick(content, regex);			
		}
		return phonenumber;
	}
	private static String matchPick(String content, String regex){
		Pattern p = Pattern.compile(regex);  
		Matcher m = p.matcher(content); 
		if (m.find()){
			return m.group(1);
		}  
		return "";
	}
}