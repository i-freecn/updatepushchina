package com.update.push.tool.utils;

import android.content.Context;

public class R_util {
	private static R_util mm= null;
	private String pk = "";
	Context activity ;
	
		
	public static R_util instance(Context act) 
	{
		if (mm ==null) 
		{
			new R_util(act);
		}
		return mm;
	}

	private R_util(Context act) {
		activity = act;
		pk = act.getPackageName();
		mm = this;
	}
	
	private int getResouceID(String type,String id){
		return activity.getResources().getIdentifier(id,type,pk);
	}

	public int getDrawable(String id){
		return getResouceID("drawable", id);
	}

	public int getID(String id){
		return getResouceID("id", id);
	}

	public int getLayout(String id){
		return getResouceID("layout", id);
	}

	public int getStyle(String id){
		return getResouceID("style", id);
	}
	public int getSring(String id){
		return getResouceID("string", id);
	}
	
	public int getColor(String id) {
		return getResouceID("color", id);
	}
}
