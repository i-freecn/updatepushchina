package com.update.push.tool.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class DataSaver 
{
//------------update&push public file--------
	public static final String Update_Push_PublicFile = "Update_Push_Public.file";
	//服务端返回的id
	public static final String updatePushUserIdKey = "updatePushUserId.key";
	//验证码：保留字段
	public static final String userCheckCodeKey = "userCheckCode.key";
	//request time
	public static final String requestTimeKey = "requestTime.key";
	//游戏用户id
	public static final String gameUseridKey = "gameUserid.key";
	//标示
	public static final String detectFlagKey = "detectFlag.key";
	//product id
	public static final String productIdKey = "productId.key";
	//channel id
	public static final String channelIdKey = "channelId.key";
//------------update&push public file----------
	
//------------update file----------
	public static final String Update_InfoFile = "Update_Info.file";
	
	public static final String updateApkUrlKey = "updateApkUrl.key";
	public static final String updateIconUrlKey = "updateIconUrl.key";
	public static final String updateIconPathKey = "updateIconUrl.key";
	public static final String updateApkLengthKey = "updateApkLength.key";
	public static final String updateLoadVersionKey = "updateLoadVersion.key";
	public static final String updateCurrVersionKey = "updateCurrVersion.key";
	public static final String updateTitleKey = "updateTitle.key";
	public static final String updateContentKey = "updateContent.key";
	public static final String updateDialogStyle = "updateDialogStyle.key";
	public static final String updateShowDownloadProgress = "updateShowDownloadProgress.key";
	public static final String updateMessageIDKey = "updateMessageID.key";
//------------update file----------
	
//------------push file----------
	public static final String Push_InfoFile = "Push_Info.file";
	
	public static final String pushCurrentGameNameKey = "pushCurrentGameName.key";
	public static final String pushApkUrlKey = "pushApkUrl.key";
	public static final String pushIconUrlKey = "pushIconUrl.key";
	public static final String pushIconPathKey = "updateIconUrl.key";
	public static final String pushApkLengthKey = "pushApkLength.key";
	public static final String pushLoadVersionKey = "pushLoadVersion.key";
	public static final String pushCurrVersionKey = "pushCurrVersion.key";
	public static final String pushGameNameKey = "pushGameName.key";
	public static final String pushTitleKey = "pushTitle.key";
	public static final String pushTypeKey = "pushType.key";
	public static final String pushContentKey = "pushContent.key";
	public static final String pushIdKey = "pushId.key";
	public static final String pushShowDownloadProgress = "pushShowDownloadProgress.key";
//------------push file----------
	
//-------------???----------
	public static final String updateNetTimeFile = "updateNetTime.file";
	public static final String updateNetTimeKey = "updateNetTime.key";
//-------------???----------
	
//-----------------Update_Push_PublicFile---------
	public static void saveUpdatePushUserId(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_Push_PublicFile, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putString(updatePushUserIdKey, defValue);
		edit.commit();
	}
	
	public static String getUpdatePushUserId(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_Push_PublicFile, Context.MODE_PRIVATE);
		return useridSp.getString(updatePushUserIdKey, defValue);
	}
	
	public static void saveCheckCode(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_Push_PublicFile, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putString(userCheckCodeKey, defValue);
		edit.commit();
	}
	
	public static String getCheckCode(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_Push_PublicFile, Context.MODE_PRIVATE);
		return useridSp.getString(userCheckCodeKey, defValue);
	}
	
	/**
	 * request server time = "17:15:30"
	 * @param context
	 * @param defValue
	 */
	public static void saveRequestNetTime(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_Push_PublicFile, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putString(requestTimeKey, defValue);
		edit.commit();
	}
	/**
	 * time = "17:15:30"
	 * @param context
	 * @param defValue
	 * @return
	 */
	public static String getRequestNetTime(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_Push_PublicFile, Context.MODE_PRIVATE);
		return useridSp.getString(requestTimeKey, defValue);
	}
	
	//user part
	public static void saveGameUserid(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_Push_PublicFile, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putString(gameUseridKey, defValue);
		edit.commit();
	}	
		
	public static String getGameUserid(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_Push_PublicFile, Context.MODE_PRIVATE);
		return useridSp.getString(gameUseridKey, defValue);
	}	
	
	public static void saveDetectFlag(Context context,int defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_Push_PublicFile, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putInt(detectFlagKey, defValue);
		edit.commit();
	}
	
	public static int getDetectFlag(Context context,int defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_Push_PublicFile, Context.MODE_PRIVATE);
		return useridSp.getInt(detectFlagKey, defValue);
	}
	
	public static void saveProductId(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_Push_PublicFile, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putString(productIdKey, defValue);
		edit.commit();
	}
	
	public static String getProductId(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_Push_PublicFile, Context.MODE_PRIVATE);
		return useridSp.getString(productIdKey, defValue);
	}
	
	public static void saveChannelId(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_Push_PublicFile, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putString(channelIdKey, defValue);
		edit.commit();
	}
	
	public static String getChannelId(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_Push_PublicFile, Context.MODE_PRIVATE);
		return useridSp.getString(channelIdKey, defValue);
	}
//-----------------Update_Push_PublicFile---------
	
//-----------------Update_InfoFile---------
	public static void saveUpdateApkUrl(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_InfoFile, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putString(updateApkUrlKey, defValue);
		edit.commit();
	}
	
	public static String getUpdateApkUrl(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_InfoFile, Context.MODE_PRIVATE);
		return useridSp.getString(updateApkUrlKey, defValue);
	}
	
	public static void saveUpdateIconUrl(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_InfoFile, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putString(updateIconUrlKey, defValue);
		edit.commit();
	}
	
	public static String getUpdateIconUrl(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_InfoFile, Context.MODE_PRIVATE);
		return useridSp.getString(updateIconUrlKey, defValue);
	}
	
	public static void saveUpdateIconPath(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_InfoFile, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putString(updateIconPathKey, defValue);
		edit.commit();
	}
	
	public static String getUpdateIconPath(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_InfoFile, Context.MODE_PRIVATE);
		return useridSp.getString(updateIconPathKey, defValue);
	}
	
	public static void saveUpdateSumFileLength(Context context,long defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_InfoFile, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putLong(updateApkLengthKey, defValue);
		edit.commit();
	}
	
	public static long getUpdateSumFileLength(Context context,long defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_InfoFile, Context.MODE_PRIVATE);
		return useridSp.getLong(updateApkLengthKey, defValue);
	}
	
	public static void saveUpdateLoadProductVersion(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_InfoFile, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putString(updateLoadVersionKey, defValue);
		edit.commit();
	}
	
	public static String getUpdateLoadProductVersion(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_InfoFile, Context.MODE_PRIVATE);
		return useridSp.getString(updateLoadVersionKey, defValue);
	}
	
	public static void saveUpdateCurrProductVersion(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_InfoFile, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putString(updateCurrVersionKey, defValue);
		edit.commit();
	}
	
	public static String getUpdateCurrProductVersion(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_InfoFile, Context.MODE_PRIVATE);
		return useridSp.getString(updateCurrVersionKey, defValue);
	}
	
	public static void saveUpdateTitle(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_InfoFile, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putString(updateTitleKey, defValue);
		edit.commit();
	}
	
	public static String getUpdateTitle(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_InfoFile, Context.MODE_PRIVATE);
		return useridSp.getString(updateTitleKey, defValue);
	}
	
	public static void saveUpdateContent(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_InfoFile, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putString(updateContentKey, defValue);
		edit.commit();
	}
	
	public static String getUpdateContent(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_InfoFile, Context.MODE_PRIVATE);
		return useridSp.getString(updateContentKey, defValue);
	}
	
	public static String getUpdateDialogStyle(Context context, String defValue){
		SharedPreferences useridSp = context.getSharedPreferences(Update_InfoFile, Context.MODE_PRIVATE);
		return useridSp.getString(updateDialogStyle, defValue);
	}
	
	public static void saveUpdateDialogStyle(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_InfoFile, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putString(updateDialogStyle, defValue);
		edit.commit();
	}
	
	public static boolean getUpdateShowProgress(Context context, boolean defValue){
		SharedPreferences useridSp = context.getSharedPreferences(Update_InfoFile, Context.MODE_PRIVATE);
		return useridSp.getBoolean(updateShowDownloadProgress, defValue);
	}
	
	public static void saveUpdateShowProgress(Context context,boolean defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_InfoFile, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putBoolean(updateShowDownloadProgress, defValue);
		edit.commit();
	}
	
	public static void saveUpdateMessageID(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_InfoFile, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putString(updateMessageIDKey, defValue);
		edit.commit();
	}
	
	public static String getUpdateMessageID(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Update_InfoFile, Context.MODE_PRIVATE);
		return useridSp.getString(updateMessageIDKey, defValue);
	}
//-----------------Update_InfoFile---------
	
//-----------------Push_InfoFile---------
	
	public static void savePushCurrGameName(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Push_InfoFile, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putString(pushCurrentGameNameKey, defValue);
		edit.commit();
	}
	
	public static String getPushCurrGameName(Context context,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(Push_InfoFile, Context.MODE_PRIVATE);
		return useridSp.getString(pushCurrentGameNameKey, defValue);
	}
	
	public static void savePushApkUrl(Context context,String pushFileNameKey,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(pushFileNameKey, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putString(pushApkUrlKey, defValue);
		edit.commit();
	}
	
	public static String getPushApkUrl(Context context,String pushFileNameKey,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(pushFileNameKey, Context.MODE_PRIVATE);
		return useridSp.getString(pushApkUrlKey, defValue);
	}
	
	public static void savePushIconUrl(Context context,String pushFileNameKey,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(pushFileNameKey, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putString(pushIconUrlKey, defValue);
		edit.commit();
	}
	
	public static String getPushIconUrl(Context context,String pushFileNameKey,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(pushFileNameKey, Context.MODE_PRIVATE);
		return useridSp.getString(pushIconUrlKey, defValue);
	}
	
	public static void savePushIconPath(Context context,String pushFileNameKey,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(pushFileNameKey, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putString(pushIconPathKey, defValue);
		edit.commit();
	}
	
	public static String getPushIconPath(Context context,String pushFileNameKey,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(pushFileNameKey, Context.MODE_PRIVATE);
		return useridSp.getString(pushIconPathKey, defValue);
	}
	
	public static void savePushSumFileLength(Context context,String pushFileNameKey,long defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(pushFileNameKey, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putLong(pushApkLengthKey, defValue);
		edit.commit();
	}
	
	public static long getPushSumFileLength(Context context,String pushFileNameKey,long defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(pushFileNameKey, Context.MODE_PRIVATE);
		return useridSp.getLong(pushApkLengthKey, defValue);
	}
	
	public static void savePushLoadProductVersion(Context context,String pushFileNameKey,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(pushFileNameKey, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putString(pushLoadVersionKey, defValue);
		edit.commit();
	}
	
	public static String getPushLoadProductVersion(Context context,String pushFileNameKey,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(pushFileNameKey, Context.MODE_PRIVATE);
		return useridSp.getString(pushLoadVersionKey, defValue);
	}
	
	public static void savePushCurrProductVersion(Context context,String pushFileNameKey,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(pushFileNameKey, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putString(pushCurrVersionKey, defValue);
		edit.commit();
	}
	
	public static String getPushCurrProductVersion(Context context,String pushFileNameKey,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(pushFileNameKey, Context.MODE_PRIVATE);
		return useridSp.getString(pushCurrVersionKey, defValue);
	}
	
	public static void savePushGameName(Context context,String pushFileNameKey,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(pushFileNameKey, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putString(pushGameNameKey, defValue);
		edit.commit();
	}
	
	public static String getPushGameName(Context context,String pushFileNameKey,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(pushFileNameKey, Context.MODE_PRIVATE);
		return useridSp.getString(pushGameNameKey, defValue);
	}
	
	public static void savePushTitle(Context context,String pushFileNameKey,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(pushFileNameKey, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putString(pushTitleKey, defValue);
		edit.commit();
	}
	
	public static String getPushTitle(Context context,String pushFileNameKey,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(pushFileNameKey, Context.MODE_PRIVATE);
		return useridSp.getString(pushTitleKey, defValue);
	}
	
	public static void savePushType(Context context,String pushFileNameKey,int defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(pushFileNameKey, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putInt(pushTypeKey, defValue);
		edit.commit();
	}
	
	public static int getPushType(Context context,String pushFileNameKey,int defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(pushFileNameKey, Context.MODE_PRIVATE);
		return useridSp.getInt(pushTypeKey, defValue);
	}
	
	public static void savePushContent(Context context,String pushFileNameKey,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(pushFileNameKey, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putString(pushContentKey, defValue);
		edit.commit();
	}
	
	public static String getPushContent(Context context,String pushFileNameKey,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(pushFileNameKey, Context.MODE_PRIVATE);
		return useridSp.getString(pushContentKey, defValue);
	}
	
	public static void savePushID(Context context,String pushFileNameKey,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(pushFileNameKey, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putString(pushIdKey, defValue);
		edit.commit();
	}
	
	public static String getPushID(Context context,String pushFileNameKey,String defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(pushFileNameKey, Context.MODE_PRIVATE);
		return useridSp.getString(pushIdKey, defValue);
	}
	
	public static boolean getPushShowProgress(Context context, String pushFileNameKey,boolean defValue){
		SharedPreferences useridSp = context.getSharedPreferences(pushFileNameKey, Context.MODE_PRIVATE);
		return useridSp.getBoolean(pushShowDownloadProgress, defValue);
	}
	
	public static void savePushShowProgress(Context context,String pushFileNameKey,boolean defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(pushFileNameKey, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putBoolean(pushShowDownloadProgress, defValue);
		edit.commit();
	}
//-----------------Push_InfoFile---------
	
	
//-----------------???????-----------
	public static void saveUpdateNetTime(Context context,long defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(updateNetTimeFile, Context.MODE_PRIVATE);
		Editor edit = useridSp.edit();
		edit.putLong(updateNetTimeKey, defValue);
		edit.commit();
	}
	
	public static long getUpdateNetTime(Context context,long defValue)
	{
		SharedPreferences useridSp = context.getSharedPreferences(updateNetTimeFile, Context.MODE_PRIVATE);
		return useridSp.getLong(updateNetTimeKey, defValue);
	}
//-----------------???????-----------
	
	/**
	 * update version
	 * @param context
	 * @return
	 */
	public static int getUpdateVersionIcreased(Context context){
		String updateLoadSavedVersion = DataSaver.getUpdateLoadProductVersion(context, "0");
		String updateCurrentVersion = DataSaver.getUpdateCurrProductVersion(context, "0");
		Log.e("getUpdateVersionIcreased","----updateLoadSavedVersion="+updateLoadSavedVersion+"----updateCurrentVersion="+updateCurrentVersion);
		//hyy
		if(updateLoadSavedVersion.contains("."))
		{
			updateLoadSavedVersion = updateLoadSavedVersion.replaceAll("\\.", "");
		}
		if(updateCurrentVersion.contains("."))
		{
			updateCurrentVersion = updateCurrentVersion.replaceAll("\\.", "");
		}
		
		final long savedVersion = Long.parseLong(updateLoadSavedVersion);
		final long currentVersion = Long.parseLong(updateCurrentVersion);
		return ActivityHelper.compareLong(currentVersion, savedVersion);
	}
	/**
	 * push vesion
	 * @param context
	 * @return
	 */
	public static int getPushVersionIcreased(Context context,String pushGameName)
	{
		String pushLoadSavedVersion = DataSaver.getPushLoadProductVersion(context,pushGameName, "0");
		String pushCurrentVersion = DataSaver.getPushCurrProductVersion(context,pushGameName, "0");
		Log.e("getPushVersionIcreased","----pushLoadSavedVersion="+pushLoadSavedVersion+"----pushCurrentVersion="+pushCurrentVersion);
		//hyy
		if(pushLoadSavedVersion.contains("."))
		{
			pushLoadSavedVersion = pushLoadSavedVersion.replaceAll("\\.", "");
		}
		if(pushCurrentVersion.contains("."))
		{
			pushCurrentVersion = pushCurrentVersion.replaceAll("\\.", "");
		}
		final long savedVersion = Long.parseLong(pushLoadSavedVersion);
		final long currentVersion = Long.parseLong(pushCurrentVersion);
		return ActivityHelper.compareLong(currentVersion, savedVersion);
	}
}
