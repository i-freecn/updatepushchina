package com.update.push.tool.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

public class PhoneUtils {

	public static final String Default = "00000";
	public static final String CMCC0 = "46000";
	public static final String CMCC2 = "46002";
	public static final String CMCC7 = "46007";
	public static final String ChinaUnicom = "46001";
	public static final String ChinaTelecom = "46003";
	
	public static TelephonyManager getTelephonyManager(Context context)
	{
		return (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
	}
	
	public static boolean NetIsAvailableCMCC(Context context)
	{
		 ConnectivityManager connManager = (ConnectivityManager) context  
				    .getSystemService(Context.CONNECTIVITY_SERVICE);  
		 NetworkInfo networkInfo = connManager.getActiveNetworkInfo();  
		 if(networkInfo == null || !networkInfo.isAvailable() ||
				 networkInfo.getType()==ConnectivityManager.TYPE_WIFI)
		 {
			  return false;
		 }	
		return true;
	}
	
	public static boolean simExists(Context context)
	{
		TelephonyManager telephonyManager = getTelephonyManager(context);
		int simState = telephonyManager.getSimState();
		if(simState == TelephonyManager.SIM_STATE_ABSENT )
		{
			return false;
		}
		return true;
	}
	
	public static String getPhoneIMSI(Context context)
	{
		TelephonyManager telephonyManager = getTelephonyManager(context);
		String _imsi = telephonyManager.getSubscriberId();
    	if(_imsi != null && !_imsi.equals(""))
    	{
    		return _imsi;
    	}else
    	{
    		return Default;
    	}
    	
	}
	
	public static String getPhoneIMEI(Context context)
	{
		TelephonyManager telephonyManager = getTelephonyManager(context);
		return telephonyManager.getDeviceId();
	}
	
	/**
	 * Phone model name
	 * @return
	 */
	public static String getPhoneType()
	{
		return Build.MODEL;
	}
	
	public static String getPhoneNumber(Context context, String imsi)
	{
		TelephonyManager telephonyManager = getTelephonyManager(context);
		String number = telephonyManager.getLine1Number();
		//if we can't get number
		if(number == null || TextUtils.isEmpty(number)){
			if(NetIsAvailableCMCC(context) && CMCC0.equals(imsi)){
				if("cmwap".equals(NetworkUtils.getRequestNetType(context))){
					return MyGetPhoneNumberUtil.sendRequest();
				}
			}
			else{
				return "";
			}
		}
		return number;
	}
	
	public static String getDeviceID(Context context) 
	{
		TelephonyManager TelephonyMgr = (TelephonyManager) context.getApplicationContext().getApplicationContext()
                .getSystemService(Context.TELEPHONY_SERVICE);
	    String m_szImei = TelephonyMgr.getDeviceId(); // Requires
	    
	    String m_szImsi = TelephonyMgr.getSubscriberId();
    	if(m_szImsi == null)
    	{
    		m_szImsi = "";
    	}
	    // READ_PHONE_STATE
	
	    // 2 compute DEVICE ID
	    String m_szDevIDShort = "35"
	    + // we make this look like a valid IMEI
	    Build.BOARD.length() % 10 + Build.BRAND.length() % 10
	    + Build.CPU_ABI.length() % 10 + Build.DEVICE.length() % 10
	    + Build.DISPLAY.length() % 10 + Build.HOST.length() % 10
	    + Build.ID.length() % 10 + Build.MANUFACTURER.length() % 10
	    + Build.MODEL.length() % 10 + Build.PRODUCT.length() % 10
	    + Build.TAGS.length() % 10 + Build.TYPE.length() % 10
	    + Build.USER.length() % 10; // 13 digits
	    // 3 android ID - unreliable
	    String m_szAndroidID = Secure.getString(context.getContentResolver(),Secure.ANDROID_ID);
	
	    //4 wifi manager, read MAC address - requires  android.permission.ACCESS_WIFI_STATE or comes as null
	    WifiManager wm = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
	    String m_szWLANMAC = wm.getConnectionInfo().getMacAddress();
	
	    //5 Bluetooth MAC address  android.permission.BLUETOOTH required
	    BluetoothAdapter m_BluetoothAdapter = null; // Local Bluetooth adapter
	    m_BluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
	    String m_szBTMAC = "";
	    if(m_BluetoothAdapter != null)
	    {
	    	m_szBTMAC = m_BluetoothAdapter.getAddress();
	    }
	    // 6 SUM THE IDs
	    String m_szLongID = m_szImei + m_szImsi + m_szDevIDShort + m_szAndroidID + m_szWLANMAC + m_szBTMAC;
	    System.out.println("----m_szLongID = "+m_szLongID);
	    MessageDigest m = null;
	    try {
	    m = MessageDigest.getInstance("MD5");
	    } catch (NoSuchAlgorithmException e) {
	    e.printStackTrace();
	                    }
	    m.update(m_szLongID.getBytes(), 0, m_szLongID.length());
	    byte p_md5Data[] = m.digest();
	
	    String m_szUniqueID = new String();
	    for (int i = 0; i < p_md5Data.length; i++) {
	    int b = (0xFF & p_md5Data[i]);
	    // if it is a single digit, make sure it have 0 in front (proper
	    // padding)
	    if (b <= 0xF)
	    m_szUniqueID += "0";
	    // add number to string
	    m_szUniqueID += Integer.toHexString(b);
	    }
	    m_szUniqueID = m_szUniqueID.toUpperCase();
	
	    Log.i("-------DeviceID------", "----"+m_szUniqueID);
	    Log.d("DeviceIdCheck", "-----DeviceId that generated:"+m_szUniqueID);
	
	    return m_szUniqueID;

	}
}
