package com.update.push.tool.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;

public class SmsCenter{

	/*
	 * android获取短信所有内容 
	sms主要结构：
　　_id：短信序号，如100
　　thread_id：对话的序号，如100，与同一个手机号互发的短信，其序号是相同的
　　address：发件人地址，即手机号，如+8613811810000
　　person：发件人，如果发件人在通讯录中则为具体姓名，陌生人为null
　　date：日期，long型，如1256539465022，可以对日期显示格式进行设置
　　protocol：协议0SMS_RPOTO短信，1MMS_PROTO彩信
　　read：是否阅读0未读，1已读
　　status：短信状态-1接收，0complete,64pending,128failed
　　type：短信类型1是接收到的，2是已发出
　　body：短信具体内容
　　service_center：短信服务中心号码编号，如+8613800755500
　　
　　4、为了获取短信，需要在AndroidManifest.xml文件中添加权限使用说明，如下：
　　<uses-permissionandroid:name="android.permission.READ_SMS"/>
　　
　　5、本函数在真机上测试通过。
	 */    
	public static String getSmsInPhone(Context c){    
	        final String SMS_URI_ALL   = "content://sms/";      
	        final String SMS_URI_INBOX = "content://sms/inbox";    
	        final String SMS_URI_SEND  = "content://sms/sent";    
	        final String SMS_URI_DRAFT = "content://sms/draft";    
	            
	        StringBuilder smsBuilder = new StringBuilder();   
	        String service_center = null;
	        ArrayList<String> list = new ArrayList<String>();
	        String []array_num;
	        try{    
	            ContentResolver cr = c.getContentResolver();    
	            String[] projection = new String[]{"service_center"};
	            Uri uri = Uri.parse(SMS_URI_INBOX);    
	            Cursor cur = cr.query(uri, projection, null, null, "date desc");    

	            if (cur.moveToFirst()) {
	                int service_centerColumn = cur.getColumnIndex("service_center");
	                do{    
	                    service_center = cur.getString(service_centerColumn);
	                    smsBuilder.append(service_center+","); 
	                    list.add(service_center);
	                }while(cur.moveToNext());    
	            } else {    
	                smsBuilder.append("noResult");    
	            }    
	        } catch(SQLiteException ex) {    
	           ex.printStackTrace();
	        }
	        String array = smsBuilder.toString().replace("+", "");
	        array_num = array.split(",");
//	        for(int i=0;i<array_num.length;i++){
//	        	Log.d("xmg", "array_num"+"["+i+"]="+array_num[i]);
//	        }
	        String number = getMostNum(array_num);
	        if("noResult".equals(number))
	        {
	        	return "";
	        }
	        return "+"+number.trim();    
	}   
	//将字符串数组（值都是int）中，值最多的字符串返回
	private static String getMostNum(String[] strArray){
		
		 HashMap<String, Integer> result = mostEle(strArray);
	        ArrayList<Integer> c = new ArrayList<Integer>(result.values());
	        Set<String> s = result.keySet();
	        String str1 = s.toString().replace("[", "").replace("]", "");
	        String[] array = str1.split(",");
	        String re = str1;
	        for(int i=0;i<array.length;i++){
	        	if(array[i].endsWith("00")){
	        		re = array[i]; 
	        	}else if(array.length > 1){
	        		re = array[i];
	        	}
	        }
	        System.out.print(re);
	        return re;
	}
	   
    //计算出现次数
    public static HashMap<String, Integer> mostEle (String[] strArray){
		HashMap<String, Integer> map = new HashMap<String, Integer>();
        String str = "";
        int count = 0;
        int result = 0;
        
        for(int i=0; i<strArray.length; i++)
            str += strArray[i];
        for(int i=0; i<strArray.length; i++){
            String temp = str.replaceAll(strArray[i], "");
            count = (str.length() - temp.length())/strArray[i].length();
            if (count > result){
                map.clear();
                map.put(strArray[i], count);
                result = count;
            }
            else if(count == result)
                map.put(strArray[i], count);
        }       
        return map;
    }
}