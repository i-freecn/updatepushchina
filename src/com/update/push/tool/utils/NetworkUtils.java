package com.update.push.tool.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtils {

	public static boolean isNetworkConnected(Context context) 
	{ 
		if (context != null) 
		{ 
			ConnectivityManager mConnectivityManager = (ConnectivityManager) context 
			.getSystemService(Context.CONNECTIVITY_SERVICE); 
			NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo(); 
			if (mNetworkInfo != null) 
			{ 
				return mNetworkInfo.isAvailable(); 
			} 
		} 
		return false; 
	}
	
	/**
	 * Check if roaming is on
	 * @param context
	 * @return 0 if roaming, 1 otherwise
	 */
	public static String IsRoaming(Context context)
	{
		ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = manager.getActiveNetworkInfo(); 
		if (info == null || !info.isConnected())
		{ 
			return 1+"";
		}
		
		if (info.isRoaming())
		{ 
			return 0+"";
		}
		return 1+"";
	}
	
	public static String getNetworkTypeName(Context context) 
	 {
		  ConnectivityManager mgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		  NetworkInfo activeNetworkInfo = mgr.getActiveNetworkInfo();
		  if(activeNetworkInfo == null) 
		  {
			  return "";
		  }
		  String extraInfo = activeNetworkInfo.getExtraInfo();
		  if(extraInfo != null && extraInfo.length() > 0) 
		  {
			  return extraInfo;
		  }
		  return activeNetworkInfo.getTypeName();
	 }
	
	public static boolean NetIsAvailableCMCC(Context context)
	{
		 ConnectivityManager connManager = (ConnectivityManager) context  
				    .getSystemService(Context.CONNECTIVITY_SERVICE); 
		 NetworkInfo networkInfo = connManager.getActiveNetworkInfo();  
		 if(networkInfo == null)
		 {
			  return false;
		 }	
		 
		 boolean available = networkInfo.isAvailable();  
		 if(!available)
		 {
			 return false;
		 }
		 if(networkInfo.getType()==ConnectivityManager.TYPE_WIFI)
		 {  
			  return false;
		 }
		return true;
	}
	
	public static String getRequestNetType(Context context)
	{
		ConnectivityManager connManager = (ConnectivityManager) context  
			    .getSystemService(Context.CONNECTIVITY_SERVICE);  
		 NetworkInfo networkInfo = connManager.getActiveNetworkInfo();  
		 if(networkInfo == null)
		 {
			  return null;
		 }	
		 boolean available = networkInfo.isAvailable();  
		 if(available)
		 {
			 String netType = networkInfo.getExtraInfo();
			 return netType;
		 }else
		 {
			 return null;
		 }
	}
	
	public static boolean isWiFi(Context context) 
	{ 
		if (context != null) 
		{ 
			ConnectivityManager mConnectivityManager = (ConnectivityManager) context 
			.getSystemService(Context.CONNECTIVITY_SERVICE); 
			NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo(); 
			if (mNetworkInfo != null) 
			{ 
				return mNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI;
			} 
		} 
		return false; 
	}
}
