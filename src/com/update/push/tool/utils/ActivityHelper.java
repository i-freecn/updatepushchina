package com.update.push.tool.utils;

import java.util.ArrayList;
import java.util.List;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

public class ActivityHelper {

	/**
	 * Is this activity(package name) at the top?
	 * @param context
	 * @param packageName
	 * @return true if this activity(package name) is on top, false otherwise
	 */
	public static boolean isTopActivity(Context context,String packageName)
	{  
		ActivityManager mActivityManager = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningTaskInfo>  tasksInfo = mActivityManager.getRunningTasks(1);  
        if(tasksInfo.size() > 0)
        {  
            if(packageName.equals(tasksInfo.get(0).topActivity.getPackageName()))
            {  
                return true;  
            }  
        }  
        return false;  
    }
	
	public static String getCurrPackageName(Context context)
	{
		String name = context.getPackageName();
		return name;
	}
	
	public static boolean existSDCard()
	{  
		if (android.os.Environment.getExternalStorageState().equals(  
			    android.os.Environment.MEDIA_MOUNTED)) 
		{  
			return true;  
		} else  
		{
			return false;  
		} 
	}
	
	/**
	 * Get all custom installed applications
	 * @param context
	 * @return
	 */
	public static ArrayList<String> getPhoneInstalledAPK(Context context)
	{
		ArrayList<String> apps = new ArrayList<String>();
		PackageManager packageManager = context.getPackageManager();  
		List<PackageInfo> pkgList = packageManager.getInstalledPackages(0);
		 for (int i = 0; i < pkgList.size(); i++)
		 {  
		        PackageInfo pkg = (PackageInfo) pkgList.get(i);  
		        if ((pkg.applicationInfo.flags & pkg.applicationInfo.FLAG_SYSTEM) <= 0) 
		        {  
		            // customs applications  
		        	String apkName = packageManager.getApplicationLabel(pkg.applicationInfo).toString();
		        	apps.add(apkName);  
		        }  
		  }  
		return apps;
	}
	
	/**
	 * Get version name from manifest file of current application
	 * @param context
	 * @return
	 */
	public static String getVersion(Context context) 
	{ 
		try 
		{ 
			PackageManager manager = context.getPackageManager(); 
			PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0); 
			String version = info.versionName; 
			return version; 
		} catch (Exception e) 
		{ 
			e.printStackTrace(); 
			return "0"; 
		} 
	}
	
	public static int compareLong(long leftValue, long rightValue){
		if(leftValue > rightValue)
			return 1;
		if(leftValue < rightValue)
			return -1;
		return 0;
	}
}
