package com.update.push.tool.utils;

import android.content.Context;

public class UpdateR 
{
	private static UpdateR UR= null;
	private String pkgName = "";
	private Context activity ;
	
		
	public static UpdateR getR(Context act) 
	{
		if (UR ==null) 
		{
			UR = new UpdateR(act);
		}
		return UR;
	}

	private UpdateR(Context act) {
		activity = act;
		pkgName = act.getPackageName();
	}
	
	private int getResouceID(String type,String id){
		return activity.getResources().getIdentifier(id,type,pkgName);
	}

	public int getDrawable(String id){
		return getResouceID("drawable", id);
	}

	public int getID(String id){
		return getResouceID("id", id);
	}

	public int getLayout(String id){
		return getResouceID("layout", id);
	}

	public int getStyle(String id){
		return getResouceID("style", id);
	}
	public int getSring(String id){
		return getResouceID("string", id);
	}
	
	public int getColor(String id) {
		return getResouceID("color", id);
	}
}
