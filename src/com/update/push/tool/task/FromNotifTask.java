package com.update.push.tool.task;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;
import android.view.View;

import com.update.push.tool.core.UpdateNotification;
import com.update.push.tool.core.Updater;
import com.update.push.tool.core.Updater.UpdaterViewManager;
import com.update.push.tool.core.UploadData;
import com.update.push.tool.utils.DataSaver;
import com.update.push.tool.utils.R_util;

/**
 * push和update下载放在这里有一些问题。
 * 应为push是没有版本的字段的，而这个类都用了update的版本来控制。(以解决)
 **/
public class FromNotifTask extends BasicTask
{
	public static final String TASK_ACTION = "from_notification_action";
	
	@Override
	protected void performExecute(Intent i, Context context,
			ResultReceiver callback) 
	{
		int updateNotificationFlag = i.getIntExtra("update_flag", 0);
		int notifFlag = i.getIntExtra("notif_flag", 0);
		String savedApkUrl = null;
		String updateMessageType = null;
		
		int versionIncreased = 0;
		
		if(updateNotificationFlag == UpdateNotification.UpdateFlag)
		{
			//hyy updateVersionIncreased
			versionIncreased = DataSaver.getUpdateVersionIcreased(context);
			savedApkUrl = DataSaver.getUpdateApkUrl(context, "");
			updateMessageType = UploadData.updateMessageType;
			if(UpdaterViewManager.getUpdateNotification() != null)
			{
				Log.e("FromNotifTask", "-------------updater UpdateNotification : != null");
				UpdateNotification n = UpdaterViewManager.getUpdateNotification();
				n.setContent("下载中...", View.INVISIBLE);
				n.sendNotify();
			}else
			{
				Log.e("FromNotifTask", "-------------updater UpdateNotification : == null");
				String title = DataSaver.getUpdateTitle(context, "");
				String content = DataSaver.getUpdateContent(context, "");
				String bitmapPath = DataSaver.getUpdateIconPath(context, null);
				Bitmap icon = (bitmapPath != null)?
						BitmapFactory.decodeFile(bitmapPath) :
						BitmapFactory.decodeResource(context.getResources(), R_util.instance(context).getDrawable("ic_launcher"));
				UpdateNotification n = Updater.UpdaterViewManager.getUpdateNotification(context, icon, title, content, updateNotificationFlag);
				n.setContent("下载中...", View.INVISIBLE);
				n.sendNotify();
			}
			
			if(versionIncreased >= 0)
			{
				DataSaver.saveUpdateLoadProductVersion(context, DataSaver.getUpdateCurrProductVersion(context, "0"));
			}
		}else if(updateNotificationFlag == UpdateNotification.PushFlag)
		{
			//hyy pushVersionIncreased
			String currentGN = DataSaver.getPushCurrGameName(context, "");
			versionIncreased = DataSaver.getPushVersionIcreased(context,currentGN);
			savedApkUrl = DataSaver.getPushApkUrl(context,currentGN, "");
			updateMessageType = UploadData.pushMessageType;
			Log.e("FromNotifTask", "---------FromNotifTask push111---->versionIncreased = "+versionIncreased);
			if(UpdaterViewManager.getPushNotification() != null)
			{
				Log.e("FromNotifTask", "-------------push UpdateNotification : != null");
				UpdateNotification n = UpdaterViewManager.getPushNotification();
				n.setContent("下载中...", View.INVISIBLE);
				n.sendNotify();
			}else
			{
				Log.e("FromNotifTask", "-------------push UpdateNotification : == null");
				String title = DataSaver.getPushTitle(context,currentGN, "");
				String content = DataSaver.getPushContent(context,currentGN, "");
				String bitmapPath = DataSaver.getPushIconPath(context, currentGN,null);
				Bitmap icon = (bitmapPath != null)?
						BitmapFactory.decodeFile(bitmapPath) :
						BitmapFactory.decodeResource(context.getResources(), R_util.instance(context).getDrawable("ic_launcher"));
				UpdateNotification n = Updater.UpdaterViewManager.getPushNotification(context, icon, title, content, updateNotificationFlag);
				n.setContent("下载中...", View.INVISIBLE);
				n.sendNotify();
			}
			
			if(versionIncreased >= 0)
			{
				DataSaver.savePushLoadProductVersion(context, currentGN,DataSaver.getPushCurrProductVersion(context, currentGN, "0"));
			}
		}
		Updater.get().uploadStats(Updater.CMD_1, DataSaver.getCheckCode(context, ""), updateMessageType);
		Updater.get().downloadFile(savedApkUrl, versionIncreased, updateNotificationFlag);
		
		sendUpdate(RESULT_OK, new Bundle());
	}

	@Override
	public void savePending(Context context, Intent intent) {
		SharedPreferences prefs = context.getSharedPreferences(TASK_ACTION, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putBoolean("pending", true);
		editor.putInt("update_flag", intent.getIntExtra("update_flag", 0));
		editor.putInt("notif_flag", intent.getIntExtra("notif_flag", 0));
		editor.commit();
	}

	@Override
	public Intent getPending(Context context) {
		Intent intent = new Intent(context, NetworkRequest.class);
		intent.setAction(TASK_ACTION);
		SharedPreferences prefs = context.getSharedPreferences(TASK_ACTION, Context.MODE_PRIVATE);
		if(prefs.getBoolean("pending", false)){ //this request is pending
			intent.putExtra("update_flag", prefs.getInt("update_flag", 0));
			intent.putExtra("notif_flag", prefs.getInt("notif_flag", 0));
			prefs.edit().putBoolean("pending", false).commit();
			return intent;
		}
		else{
			return null;
		}
	}

}
