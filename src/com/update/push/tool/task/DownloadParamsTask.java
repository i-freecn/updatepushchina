package com.update.push.tool.task;

import java.io.InputStream;
import java.net.URL;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;
import android.view.View;

import com.update.push.tool.core.UpdateNotification;
import com.update.push.tool.core.Updater;
import com.update.push.tool.utils.DataSaver;
import com.update.push.tool.utils.FileUtils;
import com.update.push.tool.utils.R_util;

public class DownloadParamsTask extends BasicTask
{
	public static final String TASK_ACTION = "download_data_action";
	
	private String updateBitmapPath;
	private String pushBitmapPath;
	private int notifFlag;
	
	
	@Override
	protected void performExecute(Intent i, final Context context,ResultReceiver callback)
	{
		Bundle resultBundle = new Bundle();
		//update
		notifFlag = DataSaver.getDetectFlag(context, 0);
		updateBitmapPath = downloadIcon(context, DataSaver.getUpdateTitle(context, ""), DataSaver.getUpdateIconUrl(context, ""));
		DataSaver.saveUpdateIconPath(context, updateBitmapPath);
		//push
		String currentGN = DataSaver.getPushCurrGameName(context, "");
		Log.e("DownloadParamsTask", "-------DownloadParamsTask push currentGN="+currentGN);
		pushBitmapPath = downloadIcon(context, DataSaver.getPushTitle(context,currentGN, ""), DataSaver.getPushIconUrl(context, currentGN,""));
		DataSaver.savePushIconPath(context,currentGN, pushBitmapPath);
		
		final String updateApk = DataSaver.getUpdateApkUrl(context, "");
		final String pushApk = DataSaver.getPushApkUrl(context, currentGN,"");
		
		//if we have update apk url and it is valid
		if(updateApk != null && !updateApk.equals(""))
		{
			//notification params
			String title = DataSaver.getUpdateTitle(context, "");
			String message = DataSaver.getUpdateContent(context, "");
			Bitmap icon = (updateBitmapPath != null)?
					BitmapFactory.decodeFile(updateBitmapPath) :
					BitmapFactory.decodeResource(context.getResources(), R_util.instance(context).getDrawable("ic_launcher"));
			if(notifFlag == Updater.DETECTION_DIALOG_FLAG)
			{
				//not call
				UpdateNotification updateNotification = Updater.UpdaterViewManager.getUpdateNotification(context, icon,
						title, message, UpdateNotification.UpdateFlag);
				
				resultBundle.putString("bitmap", updateBitmapPath);
				sendUpdate(RESULT_UPDATE, resultBundle);
			}
			else if(notifFlag == Updater.DETECTION_NOTIFICATION_FLAG)
			{
				UpdateNotification updateNotification = Updater.UpdaterViewManager.getUpdateNotification(context, icon,
						title, message, UpdateNotification.UpdateFlag);
				updateNotification.cancel();
				updateNotification.sendNotify();
				//hyy
//				Updater.get().uploadStats(Updater.CMD_3, DataSaver.getCheckCode(context, ""), UploadData.pushMessageType);
			}
		}
		
		//if we have push type and it is valid
		int pushType = DataSaver.getPushType(context,currentGN, Updater.PUSH_TYPE_ERROR);
		String title = DataSaver.getPushTitle(context,currentGN, "");
		String message = DataSaver.getPushContent(context,currentGN, "");
		Bitmap icon = (pushBitmapPath != null)?
				BitmapFactory.decodeFile(pushBitmapPath) :
				BitmapFactory.decodeResource(context.getResources(), R_util.instance(context).getDrawable("ic_launcher"));
		if(pushType == Updater.PUSH_TYPE_TEXT)//text push state
		{ 
			UpdateNotification textpushNotification = Updater.UpdaterViewManager.getPushTextNotification(context, icon, title, message, UpdateNotification.PushFlag);
			textpushNotification.cancel();
			textpushNotification.setContent(message, View.INVISIBLE);
			textpushNotification.sendNotify();
		}
		else if(pushType == Updater.PUSH_TYPE_APK)//apk state
		{ 
			if(pushApk != null && !pushApk.equals(""))//we have push apk to download
			{ 
				boolean isInstalledApk = FileUtils.isApkInstalled(context, FileUtils.getNameFromUrl(pushApk, ".apk"));
				if(!isInstalledApk)
				{
					UpdateNotification pushNotification = Updater.UpdaterViewManager.getPushNotification(context, icon, title, message, UpdateNotification.PushFlag);
					pushNotification.cancel();
					pushNotification.setContent(message, View.VISIBLE);
					pushNotification.sendNotify();
				}
			}
		}
		else//error state
		{ 
			Log.e("DownloadParamsTask", "-----DownloadParamsTask error state");
			DataSaver.savePushType(context, currentGN,Updater.PUSH_TYPE_ERROR);
		}
		
		sendUpdate(RESULT_OK, resultBundle);
	}
	
	private String downloadIcon(Context context, String name, String iconUrl)
	{
		if(iconUrl == null || iconUrl.equals(""))
		{
			return null;
		}
		 try {
		     InputStream is = (InputStream) new URL(iconUrl).getContent();
		     Bitmap bitmap = BitmapFactory.decodeStream(is);
		     is.close();
		     String result = FileUtils.saveBitmap(context, name, bitmap);
		     bitmap.recycle();
		     return result;
		 } catch (Exception e) 
		 {
			 e.printStackTrace();
		     return null;
		 }
	}

	@Override
	public void savePending(Context context, Intent intent) {
		SharedPreferences prefs = context.getSharedPreferences(TASK_ACTION, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putBoolean("pending", true);
		
		editor.commit();
	}

	@Override
	public Intent getPending(Context context) {
		Intent intent = new Intent(context, NetworkRequest.class);
		intent.setAction(TASK_ACTION);
		SharedPreferences prefs = context.getSharedPreferences(TASK_ACTION, Context.MODE_PRIVATE);
		if(prefs.getBoolean("pending", false)){ //this request is pending
			
			prefs.edit().putBoolean("pending", false).commit();
			return intent;
		}
		else{
			return null;
		}
	}
}
