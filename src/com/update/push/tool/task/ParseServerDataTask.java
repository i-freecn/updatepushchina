package com.update.push.tool.task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.update.push.tool.core.AlarmHelper;
import com.update.push.tool.core.ResponseData;
import com.update.push.tool.core.ServerData;

public class ParseServerDataTask extends BasicTask {

	public static final String TASK_ACTION = "parse_server_data_action";
	
	@Override
	protected void performExecute(Intent intent, Context context,
			ResultReceiver callback) {
		int result = BasicTask.RESULT_FAIL;
		Bundle bundle = new Bundle();
		
		try {
			ResponseData responseData = new ResponseData(context);
			JSONArray jsonArray = new JSONArray(intent.getStringExtra("data"));
			JSONObject jsonObject = null;
			//for each object in main array
			for(int i = 0; i < jsonArray.length(); i++)
			{
				//this object is update+requesttime+userid+push
				jsonObject = jsonArray.getJSONObject(i);
				//update data
				String update = jsonObject.optString(ServerData.UpdateKey, null);
				responseData.saveUpdateTypeData(update);
				//request time
				String requestTime = jsonObject.optString(ServerData.RequestTimeKey,AlarmHelper.DefaultResquestTime);
				responseData.saveRequestNetTime(requestTime);
				//user data
				String user = jsonObject.optString(ServerData.UseridKey, null);
				responseData.saveUpdateUserId(user);
				//push data
				String push = jsonObject.optString(ServerData.PushKey, null);
				responseData.savePushTypeData(push);
			}
			result = BasicTask.RESULT_OK;
		} catch (JSONException e) {
			e.printStackTrace();
		}finally{
			sendUpdate(result, bundle);
		}
	}

	@Override
	public void savePending(Context context, Intent intent) {
		SharedPreferences prefs = context.getSharedPreferences(TASK_ACTION, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putBoolean("pending", true);
		editor.putString("data", intent.getStringExtra("data"));
		editor.commit();
	}

	@Override
	public Intent getPending(Context context) {
		Intent intent = new Intent(context, NetworkRequest.class);
		intent.setAction(TASK_ACTION);
		SharedPreferences prefs = context.getSharedPreferences(TASK_ACTION, Context.MODE_PRIVATE);
		if(prefs.getBoolean("pending", false)){ //this request is pending
			intent.putExtra("data", prefs.getString("data", ""));
			prefs.edit().putBoolean("pending", false).commit();
			return intent;
		}
		else{
			return null;
		}
	}

}
