package com.update.push.tool.task;

import java.io.File;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.ResultReceiver;
import android.util.Log;
import android.view.View;

import com.update.push.tool.core.UpdateNotification;
import com.update.push.tool.core.Updater;
import com.update.push.tool.core.UploadData;
import com.update.push.tool.utils.DataSaver;
import com.update.push.tool.utils.FileUtils;

public class DownloadFileTask extends BasicTask 
{

	public static final String TASK_ACTION = "download_file_action";
	
	private String suffix_tmp = ".tmp";
	private String suffix_apk = ".apk";
	
	@Override
	protected void performExecute(Intent i, Context context,ResultReceiver callback)
	{
		PowerManager pm = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
		Bundle resultBundle = new Bundle();
		String url = i.getStringExtra("apkurl");
		int versionIncreased = i.getIntExtra("version", 0);
		int downloadFlag = i.getIntExtra("loadFlag", 0);
		resultBundle.putInt("loadFlag", downloadFlag);
		String uploadMessageType = "";
		boolean showProgress = true;
		
		UpdateNotification notification = null;
		if(downloadFlag == UpdateNotification.UpdateFlag)
		{
			Log.e("DownloadFileTask","---------DownloadFileTask downloadFlag="+downloadFlag);
			notification = Updater.UpdaterViewManager.getUpdateNotification();
			uploadMessageType = UploadData.updateMessageType;
			showProgress = DataSaver.getUpdateShowProgress(context, true);
		}
		else if(downloadFlag == UpdateNotification.PushFlag)
		{
			String currentGN = DataSaver.getPushCurrGameName(context, "");
			Log.e("DownloadFileTask","---------DownloadFileTask currentGN="+currentGN);
			notification = Updater.UpdaterViewManager.getPushNotification();
			uploadMessageType = UploadData.pushMessageType;
			showProgress = DataSaver.getPushShowProgress(context,currentGN, true);
		}
		
		if(!FileUtils.isMediaAvailable() || url == null || url.equals("") || downloadFlag == 0)
		{
			Log.e("DownloadFileTask","---------参数错误");
			sendUpdate(RESULT_FAIL, resultBundle);
			return;
		}
		
		String fileName = url.substring(url.lastIndexOf("/") + 1, url.lastIndexOf("."));
		File tmpDir = new File(FileUtils.ROOT + FileUtils.UpdaterDirName);
		tmpDir.mkdirs();
		File tmpFile = new File(tmpDir+"/"+fileName+suffix_tmp);
		File apkFile = new File(tmpDir+"/"+fileName+suffix_apk);
		resultBundle.putString("path", apkFile.getAbsolutePath());
		Log.e("Downloading file", "----path:fileName:"+fileName+" tmpFile:"+tmpFile);
		Log.e("Downloading file", "------versionIncreased44="+versionIncreased);
		
		/*
		 * Check download
		 */
		HttpURLConnection httpConn = null;
		InputStream is = null;
		long range = 0;
		long fileLength = -1;
		try 
		{
			if(versionIncreased == 0)
			{
				if(apkFile.exists())//apk file was downloaded, we can install it// ???? 
				{ 
					success(context, apkFile.getAbsolutePath(), downloadFlag);
					sendUpdate(RESULT_OK, resultBundle);
					return;
				}
				else if(tmpFile.exists())//tmp file was downloaded fully, we rename it and install
				{ 
					if(downloadFlag == UpdateNotification.UpdateFlag)
					{
						fileLength = DataSaver.getUpdateSumFileLength(context, -1);
					}
					else if(downloadFlag == UpdateNotification.PushFlag)
					{
						String currentGN = DataSaver.getPushCurrGameName(context, "");
						fileLength = DataSaver.getPushSumFileLength(context,currentGN, -1);
					}
					
					range = tmpFile.length();
					if(range == fileLength && tmpFile.renameTo(apkFile))
					{
						success(context, apkFile.getAbsolutePath(), downloadFlag);
						sendUpdate(RESULT_OK, resultBundle);
						return;
					}else if(range > fileLength && fileLength != -1)
					{
						Log.e("Download file", "range > fileLength  fileLength="+fileLength);
						tmpFile.delete();
					}
				}
				else
				{ //tmp file was downloaded partially
					//continue to download section
				}
			}
			else if(versionIncreased == 1)
			{ //server version is higher than local, downloading apk
				tmpFile.delete();
				apkFile.delete();
			}
			else{ //versionIncreased == -1, server version is lower than installed, nothing to do
				versionLowerProm(downloadFlag);
				sendUpdate(RESULT_OK, resultBundle);
				return;
			}
			
			//Making notification accept download progress if progress flag is true
			Log.e("DownloadFileTask", "-----------DownloadFileTask notification 1111");
			if(notification != null && showProgress)
			{
				Log.e("DownloadFileTask", "-----------DownloadFileTask notification call");
				notification.setProgressVisible(true);
				notification.setProgress(0, fileLength);
			}
			
			//Initiating download
			URL httpUrl = new URL(url);
			httpConn = (HttpURLConnection) httpUrl.openConnection();
			httpConn.setConnectTimeout(45*1000);
			httpConn.setRequestMethod("GET");
			httpConn.setRequestProperty("Range", "bytes="+range+"-");
			httpConn.setRequestProperty("Connection", "Keep-Alive"); 
			httpConn.connect();
			long surplusLength = httpConn.getContentLength();
			fileLength = range+surplusLength;
			
			//save file length
			if(downloadFlag == UpdateNotification.UpdateFlag)
			{
				DataSaver.saveUpdateSumFileLength(context, fileLength);
			}
			else if(downloadFlag == UpdateNotification.PushFlag)
			{
				String currentGN = DataSaver.getPushCurrGameName(context, "");
				DataSaver.savePushSumFileLength(context, currentGN,fileLength);
			}
			
			int resultCode = httpConn.getResponseCode();
			if(resultCode == HttpURLConnection.HTTP_PARTIAL || resultCode == HttpURLConnection.HTTP_OK)
			{
				is = httpConn.getInputStream();
				byte[] buff = new byte[4*1024];
				int len = 0;
				RandomAccessFile raf = new RandomAccessFile(tmpFile,"rw");
				raf.seek(range);
				int counter = 0;
				while(((len = is.read(buff))!=-1) && !isCancelled())
				{
					raf.write(buff, 0, len);
					range += len;
					counter++;
//					Log.e("Download file", "------------loading:curr:"+range+" sum:"+fileLength);
					if(counter % 20 == 0)
					{
						counter = 0;
						if(notification != null)
						{
							notification.setProgress(range, fileLength);
						}
					}
				}
				raf.close();
				
				if(range == fileLength && tmpFile.renameTo(apkFile))
				{
					Log.e("Download file", "--------load:success");
					success(context, apkFile.getAbsolutePath(), downloadFlag);
					sendUpdate(RESULT_OK, resultBundle);
				}
				else
				{
					Log.e("Download file", "--------load:fail");
					fail(downloadFlag);
					sendUpdate(RESULT_FAIL, resultBundle);
				}
				
			}
			else
			{
				Log.e("Download file", "-----resultCode error:"+resultCode);
				fail(downloadFlag);
				sendUpdate(RESULT_FAIL, resultBundle);
			}
			
		} catch (Exception e) 
		{
			Log.e("DownloadFileTask", "----------DownloadFileTask Exception:"+e.getMessage());
			fail(downloadFlag);
			sendUpdate(RESULT_FAIL, resultBundle);
		}
	}
	
	private void versionLowerProm(int downloadFlag){
		UpdateNotification notification = null;
		if(downloadFlag == UpdateNotification.UpdateFlag){
			notification = Updater.UpdaterViewManager.getUpdateNotification();
		}else if(downloadFlag == UpdateNotification.PushFlag){
			notification = Updater.UpdaterViewManager.getPushNotification();
		}
		if(notification != null)
		{
			notification.setProgressVisible(false);
			notification.setContent("版本已经是最新的啦", View.INVISIBLE);
			notification.sendNotify();
		}
	}
	
	private void success(Context context, String apkPath, int downloadFlag){
		UpdateNotification notification = null;
		String uploadMessageType = "";
		if(downloadFlag == UpdateNotification.UpdateFlag)
		{
			notification = Updater.UpdaterViewManager.getUpdateNotification();
			uploadMessageType = UploadData.updateMessageType;
		}
		else if(downloadFlag == UpdateNotification.PushFlag)
		{
			notification = Updater.UpdaterViewManager.getPushNotification();
			uploadMessageType = UploadData.pushMessageType;
		}
		if(notification != null)
		{
			notification.setProgressVisible(false);
			notification.setContent("下载完成", View.INVISIBLE);
			notification.sendNotify();
		}
		Log.e("DownloadFileTask", "----------DownloadFileTask success");
		Updater.get().uploadStats(Updater.CMD_4, DataSaver.getCheckCode(context, ""), uploadMessageType);
		FileUtils.installFile(context, apkPath);
	}
	
	private void fail(int downloadFlag)
	{
		Log.e("DownloadFileTask", "----------DownloadFileTask fail:call");
		if(downloadFlag == UpdateNotification.UpdateFlag)
		{
			Log.e("DownloadFileTask", "----------DownloadFileTask 111");
			UpdateNotification notification = Updater.UpdaterViewManager.getUpdateNotification();
			if(notification != null)
			{
				notification.cancel();
			}
		}
		else if(downloadFlag == UpdateNotification.PushFlag)
		{
			Log.e("DownloadFileTask", "----------DownloadFileTask fail:222");
			UpdateNotification notification = Updater.UpdaterViewManager.getPushNotification();
			if(notification != null)
			{
				notification.cancel();
			}
		}
		//-------------------------
	}

	@Override
	public void savePending(Context context, Intent intent) {
		SharedPreferences prefs = context.getSharedPreferences(TASK_ACTION, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putBoolean("pending", true);
		editor.putString("apkurl", intent.getStringExtra("apkurl"));
		editor.putInt("version", intent.getIntExtra("version", 0));
		editor.putInt("loadFlag", intent.getIntExtra("loadFlag", 0));
		editor.commit();
	}

	@Override
	public Intent getPending(Context context) {
		Intent intent = new Intent(context, NetworkRequest.class);
		intent.setAction(TASK_ACTION);
		SharedPreferences prefs = context.getSharedPreferences(TASK_ACTION, Context.MODE_PRIVATE);
		if(prefs.getBoolean("pending", false)){ //this request is pending
			intent.putExtra("apkurl", prefs.getString("apkurl", ""));
			intent.putExtra("version", prefs.getInt("version", 0));
			intent.putExtra("loadFlag", prefs.getInt("loadFlag", 0));
			prefs.edit().putBoolean("pending", false).commit();
			return intent;
		}
		else{
			return null;
		}
	}

}
