package com.update.push.tool.task;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.update.push.tool.core.Updater;
import com.update.push.tool.core.UploadData;

public class RequestServerTask extends BasicTask{
	
	public static final String TASK_ACTION = "request_server_data_action";
	public static enum REQUEST_RESULT {SUCCESS, ERROR}
	
	private UploadData mData;
	
	private String encoding = "utf-8";
	private String TAG = "Request server";
	
	
	@Override
	protected void performExecute(Intent intent, Context context,
			ResultReceiver callback) {
		
		String address = intent.getStringExtra("address");
		String productId = intent.getStringExtra("productId");
		String channelId = intent.getStringExtra("channelId");
		String command = intent.getStringExtra("command");
		String checkCode = intent.getStringExtra("checkCode");
		String msgType = intent.getStringExtra("msgType");
		
		mData = new UploadData(context, productId, channelId, command, checkCode, msgType);
		
		String uploadData = mData.createUploadData();
		String serverResponse = getDataFromServer(address, uploadData);
		
		//Task finished! Sending results//
		Bundle resultBundle = new Bundle();
		resultBundle.putString(RESULT_STRING, serverResponse);
		if(serverResponse == null || serverResponse.equals("") || serverResponse.equals("[]") || Updater.TEST){
			sendUpdate(RESULT_FAIL, resultBundle);
		}
		else{
			sendUpdate(RESULT_OK, resultBundle);
		}
	}

	public String getDataFromServer(String address, String uploadData)
	{
		System.out.println("-----start getDataFromServer");
		StringBuffer sb = new StringBuffer();
		URL url = null;
		HttpURLConnection httpConnection = null;
		InputStreamReader isr = null;
		BufferedReader buff = null;
		try 
		{
			url = new URL(address);
			httpConnection = (HttpURLConnection) url.openConnection();
			httpConnection.setConnectTimeout(45*1000);
			httpConnection.setDoOutput(true);
			httpConnection.setDoInput(true);
			httpConnection.setRequestMethod("POST");
			httpConnection.setUseCaches(false);
			httpConnection.setInstanceFollowRedirects(false);
			httpConnection.setRequestProperty("Content-Type", "text/xml;charset=utf-8");
			httpConnection.connect();
			DataOutputStream out = new DataOutputStream(httpConnection.getOutputStream());
			String paraContent = URLEncoder.encode(uploadData, encoding);
			out.writeBytes(paraContent);
			out.flush();
			out.close();
			int resultCode = httpConnection.getResponseCode();
			if(resultCode == HttpURLConnection.HTTP_OK)
			{
				String line = "";
				isr = new InputStreamReader(httpConnection.getInputStream(), encoding);
				buff = new BufferedReader(isr);
				while((line = buff.readLine())!=null)
				{
					sb.append(line);
				}
				Log.e(TAG, "----resultCode right:"+sb.toString());
				return sb.toString();
			}else
			{
				Log.e(TAG, "-----resultCode error:"+resultCode);
				return null;
			}
		} catch (MalformedURLException e) 
		{
			Log.e(TAG, "-----Request server MalformedURLException:"+e.getMessage());
			e.printStackTrace();
			return null;
		}catch (IOException e) 
		{
			Log.e(TAG, "----Request server IOException:"+e.getMessage());
			e.printStackTrace();
			return null;
		}finally
		{
			System.out.println("-----end getDataFromServer");
			if(httpConnection != null)
			{
				httpConnection.disconnect();
			}
			if(isr != null)
			{
				try 
				{
					isr.close();
				} catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
			if(buff != null)
			{
				try 
				{
					buff.close();
				} catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}
	}
	
	public static interface OnRequestDataListener{
		public void onRequestComplete(REQUEST_RESULT result);
	}

	@Override
	public void savePending(Context context, Intent intent) {
		SharedPreferences prefs = context.getSharedPreferences(TASK_ACTION, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putBoolean("pending", true);
		editor.putString("address", intent.getStringExtra("address"));
		editor.putString("productId", intent.getStringExtra("productId"));
		editor.putString("channelId", intent.getStringExtra("channelId"));
		editor.putString("command", intent.getStringExtra("command"));
		editor.putString("checkCode", intent.getStringExtra("checkCode"));
		editor.putString("msgType", intent.getStringExtra("msgType"));
		editor.commit();
	}
	
	@Override
	public Intent getPending(Context context) {
		Intent intent = new Intent(context, NetworkRequest.class);
		intent.setAction(TASK_ACTION);
		SharedPreferences prefs = context.getSharedPreferences(TASK_ACTION, Context.MODE_PRIVATE);
		if(prefs.getBoolean("pending", false)){ //this request is pending
			intent.putExtra("address", prefs.getString("address", ""));
			intent.putExtra("productId", prefs.getString("productId", ""));
			intent.putExtra("channelId", prefs.getString("channelId", ""));
			intent.putExtra("command", prefs.getString("command", ""));
			intent.putExtra("checkCode", prefs.getString("checkCode", ""));
			intent.putExtra("msgType", prefs.getString("msgType", ""));
			prefs.edit().putBoolean("pending", false).commit();
			return intent;
		}
		else{
			return null;
		}
	}
}