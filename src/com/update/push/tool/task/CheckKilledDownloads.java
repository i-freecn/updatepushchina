package com.update.push.tool.task;

import java.io.File;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.ResultReceiver;
import android.util.Log;

import com.update.push.tool.core.UpdateNotification;
import com.update.push.tool.core.Updater;
import com.update.push.tool.utils.DataSaver;
import com.update.push.tool.utils.FileUtils;
import com.update.push.tool.utils.NetworkUtils;
import com.update.push.tool.utils.R_util;

public class CheckKilledDownloads extends BasicTask 
{

	public static final String TASK_ACTION = "check_killed_downloads_task";
	
	@Override
	protected void performExecute(Intent i, Context context,ResultReceiver callback) 
	{
		Log.e("CheckKilledDownloads", "------CheckKilledDownloads Task");
		//check update apk download
		String tmpUpdateApkPath = FileUtils.getNameFromUrl(DataSaver.getUpdateApkUrl(context, ""), ".tmp");
		boolean updateFileExists = false;
		try 
		{
			File updateFile = new File(tmpUpdateApkPath);
			updateFileExists = updateFile.exists();
		} catch (Exception e) 
		{
			updateFileExists = false;
			Log.e("CheckKilledDownloads", "------CheckKilledDownloads update Exception = "+e.getMessage());
		}
		
		if(updateFileExists&&NetworkUtils.isWiFi(context))
		{
			Log.e("CheckKilledDownloads", "------CheckKilledDownloads Update Task");
			String title = DataSaver.getUpdateTitle(context, "");
			String content = DataSaver.getUpdateContent(context, "");
			String bitmapPath = DataSaver.getUpdateIconPath(context, null);
			Bitmap icon = (bitmapPath != null)?
					BitmapFactory.decodeFile(bitmapPath) :
					BitmapFactory.decodeResource(context.getResources(), R_util.instance(context).getDrawable("ic_launcher"));
			Updater.UpdaterViewManager.getUpdateNotification(context, icon, title, content, UpdateNotification.UpdateFlag);
			Updater.get().downloadFile(DataSaver.getUpdateApkUrl(context, ""),
					DataSaver.getUpdateVersionIcreased(context), UpdateNotification.UpdateFlag);
		}
		
		//check push apk download
		String currentGN = DataSaver.getPushCurrGameName(context, "");
		Log.e("CheckKilledDownloads", "------CheckKilledDownloads currentGN="+currentGN);
		String tmpPushApkPath = FileUtils.getNameFromUrl(DataSaver.getPushApkUrl(context,currentGN, ""), ".tmp");
		boolean pushFileExists = false;
		try 
		{
			File pushFile = new File(tmpPushApkPath);
			pushFileExists = pushFile.exists();
		} catch (Exception e) 
		{
			pushFileExists = false;
			Log.e("CheckKilledDownloads", "------CheckKilledDownloads push Exception = "+e.getMessage());
		}
		if(pushFileExists&&NetworkUtils.isWiFi(context))
		{
			Log.e("CheckKilledDownloads", "------CheckKilledDownloads Push Task");
			String title = DataSaver.getPushTitle(context,currentGN, "");
			String content = DataSaver.getPushContent(context,currentGN, "");
			String bitmapPath = DataSaver.getPushIconPath(context, currentGN,null);
			Bitmap icon = (bitmapPath != null)?
					BitmapFactory.decodeFile(bitmapPath) :
					BitmapFactory.decodeResource(context.getResources(), R_util.instance(context).getDrawable("ic_launcher"));
			Updater.UpdaterViewManager.getPushNotification(context, icon, title, content, UpdateNotification.PushFlag);
			Updater.get().downloadFile(DataSaver.getPushApkUrl(context,currentGN, ""),
					DataSaver.getPushVersionIcreased(context,currentGN), UpdateNotification.PushFlag);
		}
	}

	@Override
	public void savePending(Context context, Intent intent) {
		// TODO Auto-generated method stub

	}

	@Override
	public Intent getPending(Context context) {
		// TODO Auto-generated method stub
		return null;
	}

}
