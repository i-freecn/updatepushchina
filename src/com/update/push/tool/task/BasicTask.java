package com.update.push.tool.task;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.update.push.tool.core.Updater;
import com.update.push.tool.utils.NetworkUtils;

public abstract class BasicTask {
	public static final int RESULT_OK = 0;
	public static final int RESULT_FAIL = 1;
	public static final int RESULT_UPDATE = 2;
	
	public static final String RESULT_STRING = "result_data_string";
	public static final String CANCEL_TASK_ACTION = "cancel_current_task_action";
	
	private ResultReceiver mReceiver;
	private boolean isCancelled = false;
	
	public final void execute(Intent i, Context context, ResultReceiver callback)
	{
		mReceiver = callback;
		isCancelled = false;
		Updater.get().changeContext(context);
		if(!NetworkUtils.isNetworkConnected(context))
		{
			sendUpdate(RESULT_FAIL, new Bundle());
		}else
		{
			performExecute(i, context, callback);
		}
	}
	
	protected void sendUpdate(int resultCode, Bundle resultData){
		if(mReceiver != null)
			mReceiver.send(resultCode, resultData);
	}
	
	protected boolean isCancelled(){
		return isCancelled;
	}
	
	protected void requestCancel(){
		isCancelled = true;
	}
	
	protected abstract void performExecute(Intent i, Context context, ResultReceiver callback);
	
	public abstract void savePending(Context context, Intent intent);
	public abstract Intent getPending(Context context);
}
