package com.update.push.tool.task;

import java.util.HashMap;
import java.util.Map;

import android.app.IntentService;
import android.content.Intent;
import android.os.ResultReceiver;
import android.util.Log;

public class NetworkRequest extends IntentService 
{

	public static final String EXTRA_RESULT_RECEIVER = "extra_result_receiver";
	
//	public static boolean pushIsLoad = false; 
//	public static boolean updateIsLoad = false; 
	
	private Map<String, BasicTask> runningTasks = new HashMap<String, BasicTask>(10);
	
	public NetworkRequest() 
	{
		super("Network request");
	}

	@Override
	protected void onHandleIntent(Intent intent)
	{
//		Log.e("NetworkRequest", "----------NetworkRequest onHandleIntent Thread Name="+Thread.currentThread().getName());
		String action = intent.getAction();
		BasicTask task = null;
		String GUID = intent.getStringExtra("guid");
		final ResultReceiver receiver = intent.getParcelableExtra(EXTRA_RESULT_RECEIVER);
		//check if it is a cancel task request
		if(action.equals(BasicTask.CANCEL_TASK_ACTION))
		{
			GUID = intent.getStringExtra("guid");
			if(GUID != null)
			{
				task = runningTasks.get(GUID);
				task.requestCancel();
				runningTasks.remove(GUID);
			}
			return;
		}
		
		if(action.equals(RequestServerTask.TASK_ACTION)){
			task = new RequestServerTask();
		}
		else if(action.equals(ParseServerDataTask.TASK_ACTION)){
			task = new ParseServerDataTask();
		}
		else if(action.equals(DownloadParamsTask.TASK_ACTION)){
			task = new DownloadParamsTask();
		}
		else if(action.equals(DownloadFileTask.TASK_ACTION))
		{
			task = new DownloadFileTask();
		}
		else if(action.equals(FromNotifTask.TASK_ACTION)){
			task = new FromNotifTask();
		}
		else if(action.equals(CheckKilledDownloads.TASK_ACTION)){
			task = new CheckKilledDownloads();
		}
		
		if(task != null && GUID != null){
			runningTasks.put(GUID, task);
			task.execute(intent, getApplicationContext(), receiver);
			runningTasks.remove(GUID);
		}
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		String action = intent.getAction();
		Log.e("NetworkRequest", "----------NetworkRequest onStartCommand action:"+action);
		return super.onStartCommand(intent, flags, startId);
	}
	
}
